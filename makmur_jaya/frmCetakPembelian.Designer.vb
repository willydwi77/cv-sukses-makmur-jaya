﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCetakPembelian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvCetakPembelian = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.cetakPembelian1 = New makmur_jaya.cetakPembelian()
        Me.SuspendLayout()
        '
        'crvCetakPembelian
        '
        Me.crvCetakPembelian.ActiveViewIndex = 0
        Me.crvCetakPembelian.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvCetakPembelian.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvCetakPembelian.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvCetakPembelian.Location = New System.Drawing.Point(0, 0)
        Me.crvCetakPembelian.Name = "crvCetakPembelian"
        Me.crvCetakPembelian.ReportSource = Me.cetakPembelian1
        Me.crvCetakPembelian.ShowGotoPageButton = False
        Me.crvCetakPembelian.ShowGroupTreeButton = False
        Me.crvCetakPembelian.ShowPageNavigateButtons = False
        Me.crvCetakPembelian.ShowParameterPanelButton = False
        Me.crvCetakPembelian.ShowTextSearchButton = False
        Me.crvCetakPembelian.ShowZoomButton = False
        Me.crvCetakPembelian.Size = New System.Drawing.Size(908, 389)
        Me.crvCetakPembelian.TabIndex = 0
        Me.crvCetakPembelian.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'frmCetakPembelian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(908, 389)
        Me.Controls.Add(Me.crvCetakPembelian)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCetakPembelian"
        Me.Text = "Cetak Pembelian"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvCetakPembelian As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents cetakPembelian1 As makmur_jaya.cetakPembelian
End Class
