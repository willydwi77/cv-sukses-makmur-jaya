﻿Imports System.Data.OleDb

Public Class frmLaporanReturPenjualan

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Me.cmbNoRetur.Text = ""
    End Sub

    ' Sub untuk mengambil data dari tabel retur penjualan
    ' dengan menambahkan data dari tabel ke dalam cmbNoRetur
    Sub ambilDataReturPenjualan()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_retur_penjualan ORDER BY id DESC", conn)
            dr = cmd.ExecuteReader

            Do While dr.Read
                cmbNoRetur.Items.Add(dr.GetValue(0))
            Loop
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub frmLaporanReturPenjualan_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call ambilDataReturPenjualan()
    End Sub

    Private Sub btnTampil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTampil.Click
        Try
            ' mengambil data CrystalReportViewer berdasarkan SelectionFormula dengan dua kondisi
            ' kondisi (1) jika cmbNoRetur TIDAK kosong atau ""
            ' kondisi (2) default ketika semua ComboBox dalam keadaan kosong atau ""
            If Not Me.cmbNoRetur.Text = "" Then
                crvReturPenjualan.SelectionFormula = "{tb_retur_penjualan.id} = '" & Me.cmbNoRetur.Text & "' AND " & _
                    "{tb_retur_penjualan.tanggal_retur} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_retur_penjualan.tanggal_retur} <= CDate('" & Me.dtpAkhir.Value & "')"
            Else
                crvReturPenjualan.SelectionFormula = "{tb_retur_penjualan.tanggal_retur} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_retur_penjualan.tanggal_retur} <= CDate('" & Me.dtpAkhir.Value & "')"
            End If

            ' load file CrystalReport
            cryRPT.Load("crReturPenjualan.rpt")

            Call konfig_cr()

            ' tampilkan ke dalam CrystalReportViewer
            crvReturPenjualan.ReportSource = cryRPT
            crvReturPenjualan.RefreshReport()

            Call bersih()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class