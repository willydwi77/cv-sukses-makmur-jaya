﻿Imports System.Data.OleDb

Public Class frmDaftarPabrik
#Region "Sub"
    Sub clear()
        Me.txtPabrik.Clear()
        Me.txtPabrik.Focus()
    End Sub

    Sub btnDefault()
        Me.btnSimpan.Enabled = True
        Me.btnUbah.Enabled = False
        Me.btnHapus.Enabled = False
        Me.btnBatal.Enabled = True
    End Sub

    Sub btnOnData()
        Me.btnSimpan.Enabled = False
        Me.btnUbah.Enabled = True
        Me.btnHapus.Enabled = True
        Me.btnBatal.Enabled = True
    End Sub

    Sub kodePabrik()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_pabrik WHERE id IN (SELECT MAX(id) FROM tb_pabrik) ORDER BY id ASC", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            Dim no_urut As String
            Dim hitungan As Long

            If Not dr.HasRows Then
                no_urut = "0001"
            Else
                hitungan = dr.GetValue(0) + 1
                no_urut = Format(hitungan, "0000")
            End If

            ' DataReader Close
            dr.Close()

            Me.lblID.Text = no_urut
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Sub tampilData()
        Try
            da = New OleDbDataAdapter("SELECT * FROM tb_pabrik ORDER BY id ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_pabrik")
            dgvPabrik.DataSource = (ds.Tables("tb_pabrik"))
            dgvPabrik.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

    Private Sub frmDaftarPabrik_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
        Call kodePabrik()
        Call btnDefault()
        Call tampilData()
    End Sub

#Region "Button"
    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Try
            If Me.txtPabrik.Text = "" Then
                MsgBox("Harap masukan data yang dibutuhkan!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Dim cekData As String = "SELECT nama_pabrik FROM tb_pabrik WHERE nama_pabrik = '" & UCase(Me.txtPabrik.Text) & "'"
                cmd = New OleDbCommand(cekData, conn)
                dr = cmd.ExecuteReader
                dr.Read()

                If Not dr.HasRows Then
                    ' DataReader Close
                    dr.Close()

                    Dim simpan As String = "INSERT INTO tb_pabrik (id, nama_pabrik) " & _
                    "VALUES ('" & Me.lblID.Text & "', '" & UCase(Me.txtPabrik.Text) & "')"
                    cmd = New OleDbCommand(simpan, conn)
                    cmd.ExecuteNonQuery()

                    MsgBox("Data berhasil disimpan!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodePabrik()
                    Call btnDefault()
                    Call tampilData()
                    frmDaftarBarang.ambilPabrik()
                Else
                    ' DataReader Close
                    dr.Close()

                    MsgBox("Data sudah ada, pastikan Title tidak sama!", MsgBoxStyle.Information, "Informasi")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnUbah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUbah.Click
        Try
            Dim ubah As String = "UPDATE tb_pabrik SET " & _
                "nama_pabrik = '" & UCase(Me.txtPabrik.Text) & "' " & _
                "WHERE id = '" & Me.lblID.Text & "'"
            cmd = New OleDbCommand(ubah, conn)
            cmd.ExecuteNonQuery()

            MsgBox("Data berhasil diperbaharui!", MsgBoxStyle.Information, "Informasi")

            Call clear()
            Call kodePabrik()
            Call btnDefault()
            Call tampilData()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        Try
            If MsgBox("Apakah Anda ingin menghapus data '" & UCase(Me.txtPabrik.Text) & "'?", MsgBoxStyle.YesNo, "Informasi") = MsgBoxResult.Yes Then
                Dim cekData As String = "SELECT id FROM tb_pabrik WHERE id = '" & Me.lblID.Text & "'"
                cmd = New OleDbCommand(cekData, conn)
                dr = cmd.ExecuteReader
                dr.Read()

                If dr.HasRows Then
                    ' DataReader Close
                    dr.Close()

                    Dim hapus As String = "DELETE FROM tb_pabrik WHERE id = '" & Me.lblID.Text & "'"
                    cmd = New OleDbCommand(hapus, conn)
                    cmd.ExecuteNonQuery()

                    MsgBox("Data sudah dihapus!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodePabrik()
                    Call btnDefault()
                    Call tampilData()
                Else
                    ' DataReader Close
                    dr.Close()

                    MsgBox("Maaf, data yang dimaksud tidak ada!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodePabrik()
                    Call btnDefault()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatal.Click
        Call clear()
        Call kodePabrik()
        Call btnDefault()
    End Sub
#End Region

    Private Sub txtPencarian_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPencarian.TextChanged
        Try
            If Me.txtPencarian.Text = "" Then
                Call tampilData()
            Else
                Dim cari As String = "SELECT * FROM tb_pabrik WHERE " & _
                    "nama_pabrik LIKE '%" & Me.txtPencarian.Text & "%' ORDER BY id ASC"
                da = New OleDbDataAdapter(cari, conn)
                dt.Clear()
                da.Fill(dt)
                dgvPabrik.DataSource = dt
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub dgvPabrik_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPabrik.CellContentDoubleClick
        Try
            If IsDBNull(dgvPabrik.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                MsgBox("Tidak ada data yang dipilih!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Me.lblID.Text = dgvPabrik.Rows(e.RowIndex).Cells(0).Value
                Me.txtPabrik.Text = dgvPabrik.Rows(e.RowIndex).Cells(1).Value

                Call btnOnData()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class