﻿Imports System.Data.OleDb

Public Class frmDaftarSupplier
#Region "Sub"
    Sub clear()
        Call nilaiAwal()

        Me.txtTitle.Clear()
        Me.txtNama.Clear()
        Me.txtAlamat.Clear()
        Me.txtKota.Clear()
        Me.txtTelepon.Clear()

        Me.txtTitle.Focus()
    End Sub

    Sub nilaiAwal()
        Me.txtSaldo.Text = 0
    End Sub

    Sub btnDefault()
        Me.btnSimpan.Enabled = True
        Me.btnUbah.Enabled = False
        Me.btnHapus.Enabled = False
        Me.btnBatal.Enabled = True
    End Sub

    Sub btnOnData()
        Me.btnSimpan.Enabled = False
        Me.btnUbah.Enabled = True
        Me.btnHapus.Enabled = True
        Me.btnBatal.Enabled = True
    End Sub

    Sub kodeSupplier()
        'Try
        cmd = New OleDbCommand("SELECT id FROM tb_supplier WHERE id IN (SELECT MAX(id) FROM tb_supplier) ORDER BY id ASC", conn)
        dr = cmd.ExecuteReader
        dr.Read()

        Dim no_urut As String
        Dim hitungan As Long

        If Not dr.HasRows Then
            no_urut = Format(Now, "yyyyMMdd") + "001"
        Else
            If Microsoft.VisualBasic.Left(dr.GetString(0), 8) <> Format(Now, "yyyyMMdd") Then
                no_urut = Format(Now, "yyyyMM") + "001"
            Else
                hitungan = Microsoft.VisualBasic.Right(dr.GetString(0), 3) + 1
                no_urut = Format(Now, "yyyyMMdd") + Microsoft.VisualBasic.Right("000" & hitungan, 3)
            End If
        End If

        ' DataReader Close
        dr.Close()

        Me.lblID.Text = no_urut
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        'End Try
    End Sub
#End Region

    Sub tampilData()
        Try
            da = New OleDbDataAdapter("SELECT * FROM tb_supplier ORDER BY id ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_supplier")
            dgvSupplier.DataSource = (ds.Tables("tb_supplier"))
            dgvSupplier.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub frmDaftarSupplier_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
        Call nilaiAwal()
        Call kodeSupplier()
        Call tampilData()
        Call btnDefault()
    End Sub

#Region "Button"
    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Try
            If Me.txtTitle.Text = "" Or Me.txtNama.Text = "" Then
                MsgBox("Harap masukan data yang dibutuhkan!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Dim cekData As String = "SELECT title FROM tb_supplier WHERE title = '" & Me.txtTitle.Text & "'"
                cmd = New OleDbCommand(cekData, conn)
                dr = cmd.ExecuteReader
                dr.Read()

                If Not dr.HasRows Then
                    ' DataReader Close
                    dr.Close()

                    Dim simpan As String = "INSERT INTO tb_supplier (id, title, nama_supplier, alamat, kota, telepon, saldo_awal) " & _
                    "VALUES ('" & Me.lblID.Text & "', '" & UCase(Me.txtTitle.Text) & "', '" & UCase(Me.txtNama.Text) & "', '" & UCase(Me.txtAlamat.Text) & "', '" & UCase(Me.txtKota.Text) & "', '" & Me.txtTelepon.Text & "', " & CDbl(Me.txtSaldo.Text) & ")"
                    cmd = New OleDbCommand(simpan, conn)
                    cmd.ExecuteNonQuery()

                    MsgBox("Data berhasil disimpan!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodeSupplier()
                    Call btnDefault()
                    Call tampilData()
                Else
                    ' DataReader Close
                    dr.Close()

                    MsgBox("Data sudah ada, pastikan Title tidak sama!", MsgBoxStyle.Information, "Informasi")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnUbah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUbah.Click
        Try
            Dim ubah As String = "UPDATE tb_supplier SET " & _
                "title = '" & UCase(Me.txtTitle.Text) & "', " & _
                "nama_supplier = '" & UCase(Me.txtNama.Text) & "', " & _
                "alamat = '" & UCase(Me.txtAlamat.Text) & "', " & _
                "kota = '" & UCase(Me.txtKota.Text) & "', " & _
                "telepon = '" & UCase(Me.txtTelepon.Text) & "', " & _
                "saldo_awal = " & CDbl(Me.txtSaldo.Text) & " " & _
                "WHERE id = '" & Me.lblID.Text & "'"
            cmd = New OleDbCommand(ubah, conn)
            cmd.ExecuteNonQuery()

            MsgBox("Data berhasil diperbaharui!", MsgBoxStyle.Information, "Informasi")

            Call clear()
            Call kodeSupplier()
            Call btnDefault()
            Call tampilData()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        Try
            If MsgBox("Apakah Anda ingin menghapus data '" & UCase(Me.txtTitle.Text) & "'?", MsgBoxStyle.YesNo, "Informasi") = MsgBoxResult.Yes Then
                Dim cekData As String = "SELECT id FROM tb_supplier WHERE id = '" & Me.lblID.Text & "'"
                cmd = New OleDbCommand(cekData, conn)
                dr = cmd.ExecuteReader
                dr.Read()

                If dr.HasRows Then
                    ' DataReader Close
                    dr.Close()

                    Dim hapus As String = "DELETE FROM tb_supplier WHERE id = '" & Me.lblID.Text & "'"
                    cmd = New OleDbCommand(hapus, conn)
                    cmd.ExecuteNonQuery()

                    MsgBox("Data sudah dihapus!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodeSupplier()
                    Call btnDefault()
                    Call tampilData()
                Else
                    ' DataReader Close
                    dr.Close()

                    MsgBox("Maaf, data yang dimaksud tidak ada!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call btnDefault()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatal.Click
        Call clear()
        Call kodeSupplier()
        Call btnDefault()
    End Sub
#End Region

    Private Sub txtPencarian_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPencarian.TextChanged
        Try
            If Me.txtPencarian.Text = "" Then
                Call tampilData()
            Else
                Dim cari As String = "SELECT * FROM tb_supplier WHERE " & _
                    "title LIKE '%" & Me.txtPencarian.Text & "%' OR " & _
                    "nama_supplier LIKE '%" & Me.txtPencarian.Text & "%' ORDER BY id ASC"
                da = New OleDbDataAdapter(cari, conn)
                dt.Clear()
                da.Fill(dt)
                dgvSupplier.DataSource = dt
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub dgvSupplier_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSupplier.CellContentDoubleClick
        Try
            If IsDBNull(dgvSupplier.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                MsgBox("Tidak ada data yang dipilih!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Me.lblID.Text = dgvSupplier.Rows(e.RowIndex).Cells(0).Value
                Me.txtTitle.Text = dgvSupplier.Rows(e.RowIndex).Cells(1).Value
                Me.txtNama.Text = dgvSupplier.Rows(e.RowIndex).Cells(2).Value
                Me.txtAlamat.Text = dgvSupplier.Rows(e.RowIndex).Cells(3).Value
                Me.txtKota.Text = dgvSupplier.Rows(e.RowIndex).Cells(4).Value
                Me.txtTelepon.Text = dgvSupplier.Rows(e.RowIndex).Cells(5).Value
                Me.txtSaldo.Text = FormatCurrency(dgvSupplier.Rows(e.RowIndex).Cells(6).Value)

                Call btnOnData()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

#Region "Identifier_Number"
    Private Sub txtTelepon_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelepon.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtSaldo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSaldo.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub
#End Region

#Region "Format_Currency"
    Private Sub txtSaldo_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSaldo.Leave
        If Me.txtSaldo.Text = "" Or Not IsNumeric(Me.txtSaldo.Text) Then
            Exit Sub
        End If

        Me.txtSaldo.Text = FormatCurrency(CDbl(Me.txtSaldo.Text))
    End Sub
#End Region
End Class