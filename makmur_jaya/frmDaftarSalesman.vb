﻿Imports System.Data.OleDb

Public Class frmDaftarSalesman
#Region "Sub"
    Sub clear()
        Call nilaiAwal()

        Me.txtKaryawan.Clear()
        Me.txtAlamat.Clear()
        Me.txtKota.Clear()
        Me.txtCabang.Clear()
        Me.txtArea.Clear()

        Me.txtKaryawan.Focus()
    End Sub

    Sub nilaiAwal()
        Me.txtTarget.Text = 0
    End Sub

    Sub btnDefault()
        Me.btnSimpan.Enabled = True
        Me.btnUbah.Enabled = False
        Me.btnHapus.Enabled = False
        Me.btnBatal.Enabled = True
    End Sub

    Sub btnOnData()
        Me.btnSimpan.Enabled = False
        Me.btnUbah.Enabled = True
        Me.btnHapus.Enabled = True
        Me.btnBatal.Enabled = True
    End Sub

    Sub kodeSalesman()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_karyawan WHERE id IN (SELECT MAX(id) FROM tb_karyawan) ORDER BY id ASC", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            Dim no_urut As String
            Dim hitungan As Long

            If Not dr.HasRows Then
                no_urut = "0001"
            Else
                hitungan = dr.GetValue(0) + 1
                no_urut = Format(hitungan, "0000")
            End If

            ' DataReader Close
            dr.Close()

            Me.lblID.Text = no_urut
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Sub tampilData()
        Try
            da = New OleDbDataAdapter("SELECT * FROM tb_karyawan ORDER BY id ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_karyawan")
            dgvSalesman.DataSource = (ds.Tables("tb_karyawan"))
            dgvSalesman.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

    Private Sub frmDaftarSalesman_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
        Call nilaiAwal()
        Call kodeSalesman()
        Call tampilData()
        Call btnDefault()
    End Sub

#Region "Button"
    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Try
            If Me.txtKaryawan.Text = "" Then
                MsgBox("Harap masukan data yang dibutuhkan!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Dim cekData As String = "SELECT nama_karyawan FROM tb_karyawan WHERE nama_karyawan = '" & Me.txtKaryawan.Text & "'"
                cmd = New OleDbCommand(cekData, conn)
                dr = cmd.ExecuteReader
                dr.Read()

                If Not dr.HasRows Then
                    ' DataReader Close
                    dr.Close()

                    Dim simpan As String = "INSERT INTO tb_karyawan (id, nama_karyawan, alamat, kota, cabang, area_penjualan, target_penjualan) " & _
                    "VALUES ('" & Me.lblID.Text & "', '" & UCase(Me.txtKaryawan.Text) & "', '" & UCase(Me.txtAlamat.Text) & "', '" & UCase(Me.txtKota.Text) & "', '" & UCase(Me.txtCabang.Text) & "', '" & UCase(Me.txtArea.Text) & "', " & CDbl(Me.txtTarget.Text) & ")"
                    cmd = New OleDbCommand(simpan, conn)
                    cmd.ExecuteNonQuery()

                    MsgBox("Data berhasil disimpan!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call btnDefault()
                    Call kodeSalesman()
                    Call tampilData()
                Else
                    ' DataReader Close
                    dr.Close()

                    MsgBox("Data sudah ada, pastikan Nama Karyawan tidak sama!", MsgBoxStyle.Information, "Informasi")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnUbah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUbah.Click
        Try
            Dim ubah As String = "UPDATE tb_karyawan SET " & _
                "nama_karyawan = '" & UCase(Me.txtKaryawan.Text) & "', " & _
                "alamat = '" & UCase(Me.txtAlamat.Text) & "', " & _
                "kota = '" & UCase(Me.txtKota.Text) & "', " & _
                "cabang = '" & UCase(Me.txtCabang.Text) & "', " & _
                "area_penjualan = '" & UCase(Me.txtArea.Text) & "', " & _
                "target_penjualan = " & CDbl(Me.txtTarget.Text) & " " & _
                "WHERE id = '" & Me.lblID.Text & "'"
            cmd = New OleDbCommand(ubah, conn)
            cmd.ExecuteNonQuery()

            MsgBox("Data berhasil diperbaharui!", MsgBoxStyle.Information, "Informasi")

            Call clear()
            Call btnDefault()
            Call kodeSalesman()
            Call tampilData()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        Try
            If MsgBox("Apakah Anda ingin menghapus data '" & UCase(Me.txtKaryawan.Text) & "'?", MsgBoxStyle.YesNo, "Informasi") = MsgBoxResult.Yes Then
                Dim cekData As String = "SELECT id FROM tb_karyawan WHERE id = '" & Me.lblID.Text & "'"
                cmd = New OleDbCommand(cekData, conn)
                dr = cmd.ExecuteReader
                dr.Read()

                If dr.HasRows Then
                    ' DataReader Close
                    dr.Close()

                    Dim hapus As String = "DELETE FROM tb_karyawan WHERE id = '" & Me.lblID.Text & "'"
                    cmd = New OleDbCommand(hapus, conn)
                    cmd.ExecuteNonQuery()

                    MsgBox("Data sudah dihapus!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call btnDefault()
                    Call kodeSalesman()
                    Call tampilData()
                Else
                    ' DataReader Close
                    dr.Close()

                    MsgBox("Maaf, data yang dimaksud tidak ada!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodeSalesman()
                    Call btnDefault()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatal.Click
        Call clear()
        Call kodeSalesman()
        Call btnDefault()
    End Sub
#End Region

    Private Sub txtPencarian_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPencarian.TextChanged
        Try
            If Me.txtPencarian.Text = "" Then
                Call tampilData()
            Else
                Dim cari As String = "SELECT * FROM tb_karyawan WHERE " & _
                    "nama_karyawan LIKE '%" & Me.txtPencarian.Text & "%' OR " & _
                    "cabang LIKE '%" & Me.txtPencarian.Text & "%' OR " & _
                    "area_penjualan LIKE '%" & Me.txtPencarian.Text & "%' ORDER BY id ASC"
                da = New OleDbDataAdapter(cari, conn)
                dt.Clear()
                da.Fill(dt)
                dgvSalesman.DataSource = dt
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub dgvSalesman_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSalesman.CellContentDoubleClick
        Try
            If IsDBNull(dgvSalesman.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                MsgBox("Tidak ada data yang dipilih!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Me.lblID.Text = dgvSalesman.Rows(e.RowIndex).Cells(0).Value
                Me.txtKaryawan.Text = dgvSalesman.Rows(e.RowIndex).Cells(1).Value
                Me.txtAlamat.Text = dgvSalesman.Rows(e.RowIndex).Cells(2).Value
                Me.txtKota.Text = dgvSalesman.Rows(e.RowIndex).Cells(3).Value
                Me.txtCabang.Text = dgvSalesman.Rows(e.RowIndex).Cells(4).Value
                Me.txtArea.Text = dgvSalesman.Rows(e.RowIndex).Cells(5).Value
                Me.txtTarget.Text = FormatCurrency(dgvSalesman.Rows(e.RowIndex).Cells(6).Value)

                Call btnOnData()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

#Region "Identifier_Number"
    Private Sub txtTarget_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTarget.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub
#End Region

#Region "Format_Currency"
    Private Sub txtTarget_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTarget.Leave
        If Me.txtTarget.Text = "" Or Not IsNumeric(Me.txtTarget.Text) Then
            Exit Sub
        End If

        Me.txtTarget.Text = FormatCurrency(CDbl(Me.txtTarget.Text))
    End Sub
#End Region
End Class