﻿Imports System.Data.OleDb

Public Class frmLaporanBarang

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Me.cmbBarang.Text = ""
    End Sub

    ' Sub untuk mengambil data dari tabel barang
    ' dengan menambahkan data dari tabel ke dalam cmbBarang
    Sub ambilDataBarang()
        Try
            cmd = New OleDbCommand("SELECT nama_barang FROM tb_barang ORDER BY nama_barang ASC", conn)
            dr = cmd.ExecuteReader

            Do While dr.Read
                Me.cmbBarang.Items.Add(dr.GetValue(0))
            Loop
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' main load
    Private Sub frmLaporanBarang_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call ambilDataBarang()
    End Sub

    Private Sub cmbBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBarang.SelectedIndexChanged
        Try
            ' mengambil data id barang berdasarkan nama barang yang terpilih di ComboBox Barang
            cmd = New OleDbCommand("SELECT id FROM tb_barang WHERE nama_barang = ?", conn)
            cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = Me.cmbBarang.Text

            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDBarang.Text = dr.GetValue(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnTampil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTampil.Click
        Try
            ' mengambil data CrystalReportViewer berdasarkan SelectionFormula dengan dua kondisi
            ' kondisi (1) jika cmbBarang TIDAK kosong atau ""
            ' kondisi (2) default ketika semua ComboBox dalam keadaan kosong atau ""
            If Not Me.cmbBarang.Text = "" Then
                crvBarang.SelectionFormula = "{tb_barang.id} = '" & Me.panelIDBarang.Text & "' AND " & _
                    "{tb_penjualan.tanggal_penjualan} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_penjualan.tanggal_penjualan} <= CDate('" & Me.dtpAkhir.Value & "')"
            Else
                crvBarang.SelectionFormula = "{tb_penjualan.tanggal_penjualan} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_penjualan.tanggal_penjualan} <= CDate('" & Me.dtpAkhir.Value & "')"
            End If

            ' load file CrystalReport
            cryRPT.Load("crBarang.rpt")

            Call konfig_cr()

            ' tampilkan ke dalam CrystalReportViewer
            crvBarang.ReportSource = cryRPT
            crvBarang.RefreshReport()

            Call bersih()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class