﻿Imports System.Data.OleDb

Public Class frmLaporanPembelian

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Me.cmbNoFaktur.Text = ""
        Me.cmbSupplier.Text = ""
    End Sub

    ' Sub untuk mengambil data dari tabel pembelian
    ' dengan menambahkan data dari tabel ke dalam cmbNoFaktur
    Sub ambilDataPembelian()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_pembelian ORDER BY id DESC", conn)
            dr = cmd.ExecuteReader

            Do While dr.Read
                cmbNoFaktur.Items.Add(dr.GetValue(0))
            Loop
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' Sub untuk mengambil data dari tabel supplier
    ' dengan menambahkan data dari tabel ke dalam cmbSupplier
    Sub ambilDataSupplier()
        Try
            cmd = New OleDbCommand("SELECT nama_supplier FROM tb_supplier ORDER BY nama_supplier ASC", conn)
            dr = cmd.ExecuteReader

            Do While dr.Read
                cmbSupplier.Items.Add(dr.GetValue(0))
            Loop
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' main load
    Private Sub frmLaporanPembelian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call ambilDataPembelian()
        Call ambilDataSupplier()
    End Sub

    Private Sub cmbSupplier_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSupplier.SelectedIndexChanged
        Try
            ' mengambil data id supplier berdasarkan nama supplier yang terpilih di ComboBox Supplier
            cmd = New OleDbCommand("SELECT id FROM tb_supplier WHERE nama_supplier = ?", conn)
            cmd.Parameters.Add("@nama_supplier", OleDbType.VarChar).Value = Me.cmbSupplier.Text

            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDSupplier.Text = dr.GetValue(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnTampil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTampil.Click
        Try
            ' mengambil data CrystalReportViewer berdasarkan SelectionFormula dengan empat kondisi
            ' kondisi (1) jika cmbNoFaktur TIDAK kosong atau ""
            ' kondisi (2) jika cmbSupplier TIDAK kosong atau ""
            ' kondisi (3) jika cmbNoFaktur Dan cmbSupplier TIDAK kosong atau ""
            ' kondisi (4) default ketika semua ComboBox dalam keadaan kosong atau ""
            If Not Me.cmbNoFaktur.Text = "" Then
                crvPembelian.SelectionFormula = "{tb_pembelian.id} = '" & Me.cmbNoFaktur.Text & "' AND " & _
                    "{tb_pembelian.tanggal_pembelian} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_pembelian.tanggal_pembelian} <= CDate('" & Me.dtpAkhir.Value & "')"
            ElseIf Not Me.cmbSupplier.Text = "" Then
                crvPembelian.SelectionFormula = "{tb_supplier.id} = '" & Me.panelIDSupplier.Text & "' AND " & _
                    "{tb_pembelian.tanggal_pembelian} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_pembelian.tanggal_pembelian} <= CDate('" & Me.dtpAkhir.Value & "')"
            ElseIf Not Me.cmbNoFaktur.Text = "" And Not Me.cmbSupplier.Text = "" Then
                crvPembelian.SelectionFormula = "{tb_pembelian.id} = '" & Me.cmbNoFaktur.Text & "' AND {tb_supplier.id} = '" & Me.panelIDSupplier.Text & "' AND " & _
                    "{tb_pembelian.tanggal_pembelian} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_pembelian.tanggal_pembelian} <= CDate('" & Me.dtpAkhir.Value & "')"
            Else
                crvPembelian.SelectionFormula = "{tb_pembelian.tanggal_pembelian} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_pembelian.tanggal_pembelian} <= CDate('" & Me.dtpAkhir.Value & "')"
            End If

            ' load file CrystalReport
            cryRPT.Load("crPembelian.rpt")

            Call konfig_cr()

            ' tampilkan ke dalam CrystalReportViewer
            crvPembelian.ReportSource = cryRPT
            crvPembelian.RefreshReport()

            Call bersih()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class