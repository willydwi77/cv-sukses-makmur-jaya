﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCetakReturPenjualan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvCetakReturPenjualan = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.cetakReturPenjualan1 = New makmur_jaya.cetakReturPenjualan()
        Me.SuspendLayout()
        '
        'crvCetakReturPenjualan
        '
        Me.crvCetakReturPenjualan.ActiveViewIndex = 0
        Me.crvCetakReturPenjualan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvCetakReturPenjualan.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvCetakReturPenjualan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvCetakReturPenjualan.Location = New System.Drawing.Point(0, 0)
        Me.crvCetakReturPenjualan.Name = "crvCetakReturPenjualan"
        Me.crvCetakReturPenjualan.ReportSource = Me.cetakReturPenjualan1
        Me.crvCetakReturPenjualan.ShowGotoPageButton = False
        Me.crvCetakReturPenjualan.ShowGroupTreeButton = False
        Me.crvCetakReturPenjualan.ShowPageNavigateButtons = False
        Me.crvCetakReturPenjualan.ShowParameterPanelButton = False
        Me.crvCetakReturPenjualan.ShowTextSearchButton = False
        Me.crvCetakReturPenjualan.ShowZoomButton = False
        Me.crvCetakReturPenjualan.Size = New System.Drawing.Size(888, 369)
        Me.crvCetakReturPenjualan.TabIndex = 3
        Me.crvCetakReturPenjualan.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'frmCetakReturPenjualan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(888, 369)
        Me.Controls.Add(Me.crvCetakReturPenjualan)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCetakReturPenjualan"
        Me.Text = "Cetak Retur Penjualan"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvCetakReturPenjualan As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents cetakReturPenjualan1 As makmur_jaya.cetakReturPenjualan
End Class
