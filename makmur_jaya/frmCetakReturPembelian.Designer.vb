﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCetakReturPembelian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvCetakReturPembelian = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.cetakReturPembelian1 = New makmur_jaya.cetakReturPembelian()
        Me.SuspendLayout()
        '
        'crvCetakReturPembelian
        '
        Me.crvCetakReturPembelian.ActiveViewIndex = 0
        Me.crvCetakReturPembelian.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvCetakReturPembelian.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvCetakReturPembelian.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvCetakReturPembelian.Location = New System.Drawing.Point(0, 0)
        Me.crvCetakReturPembelian.Name = "crvCetakReturPembelian"
        Me.crvCetakReturPembelian.ReportSource = Me.cetakReturPembelian1
        Me.crvCetakReturPembelian.ShowGotoPageButton = False
        Me.crvCetakReturPembelian.ShowGroupTreeButton = False
        Me.crvCetakReturPembelian.ShowPageNavigateButtons = False
        Me.crvCetakReturPembelian.ShowParameterPanelButton = False
        Me.crvCetakReturPembelian.ShowTextSearchButton = False
        Me.crvCetakReturPembelian.ShowZoomButton = False
        Me.crvCetakReturPembelian.Size = New System.Drawing.Size(898, 379)
        Me.crvCetakReturPembelian.TabIndex = 2
        Me.crvCetakReturPembelian.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'frmCetakReturPembelian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(898, 379)
        Me.Controls.Add(Me.crvCetakReturPembelian)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCetakReturPembelian"
        Me.Text = "Cetak Retur Pembelian"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvCetakReturPembelian As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents cetakReturPembelian1 As makmur_jaya.cetakReturPembelian
End Class
