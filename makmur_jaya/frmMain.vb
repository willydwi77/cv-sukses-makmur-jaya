﻿Public Class frmMain

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        ' konfirmasi ketika saat keluar dari program
        If MsgBox("Apakah Anda ingin keluar?", MsgBoxStyle.YesNo, "Informasi") = MsgBoxResult.Yes Then
            frmDaftarBarang.Close()
            frmDaftarLangganan.Close()
            frmDaftarSalesman.Close()
            frmDaftarSupplier.Close()
            frmPembelian.Close()
            frmPenjualan.Close()
            frmLaporanPembelian.Close()
            frmLaporanPenjualan.Close()

            Me.Close()
            frmLogin.Show()
            frmLogin.txtUsername.Focus()
        End If
    End Sub

    Private Sub DaftarBarangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaftarBarangToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmDaftarBarang.TopLevel = False
        panelForm.Controls.Add(frmDaftarBarang)
        frmDaftarBarang.Show()
    End Sub

    Private Sub DaftarSalesmanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaftarSalesmanToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmDaftarSalesman.TopLevel = False
        panelForm.Controls.Add(frmDaftarSalesman)
        frmDaftarSalesman.Show()
    End Sub

    Private Sub DaftarSupplierToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaftarSupplierToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmDaftarSupplier.TopLevel = False
        panelForm.Controls.Add(frmDaftarSupplier)
        frmDaftarSupplier.Show()
    End Sub

    Private Sub DaftarLanggananToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaftarLanggananToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmDaftarLangganan.TopLevel = False
        panelForm.Controls.Add(frmDaftarLangganan)
        frmDaftarLangganan.Show()
    End Sub

    Private Sub PenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PenjualanToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmPenjualan.TopLevel = False
        panelForm.Controls.Add(frmPenjualan)
        frmPenjualan.Show()
    End Sub

    Private Sub PembelianToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PembelianToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmPembelian.TopLevel = False
        panelForm.Controls.Add(frmPembelian)
        frmPembelian.Show()
    End Sub

    Private Sub ReturPembelianToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReturPembelianToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmReturPembelian.TopLevel = False
        panelForm.Controls.Add(frmReturPembelian)
        frmReturPembelian.Show()
    End Sub

    Private Sub ReturPenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReturPenjualanToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmReturPenjualan.TopLevel = False
        panelForm.Controls.Add(frmReturPenjualan)
        frmReturPenjualan.Show()
    End Sub

    Private Sub PembelianToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PembelianToolStripMenuItem1.Click
        ' load form ke dalam panelForm
        frmLaporanPembelian.TopLevel = False
        panelForm.Controls.Add(frmLaporanPembelian)
        frmLaporanPembelian.Show()
    End Sub

    Private Sub PenjualanToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PenjualanToolStripMenuItem1.Click
        ' load form ke dalam panelForm
        frmLaporanPenjualan.TopLevel = False
        panelForm.Controls.Add(frmLaporanPenjualan)
        frmLaporanPenjualan.Show()
    End Sub

    Private Sub ReturPembelianToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReturPembelianToolStripMenuItem1.Click
        ' load form ke dalam panelForm
        frmLaporanReturPembelian.TopLevel = False
        panelForm.Controls.Add(frmLaporanReturPembelian)
        frmLaporanReturPembelian.Show()
    End Sub

    Private Sub ReturPenjualanToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReturPenjualanToolStripMenuItem1.Click
        ' load form ke dalam panelForm
        frmLaporanReturPenjualan.TopLevel = False
        panelForm.Controls.Add(frmLaporanReturPenjualan)
        frmLaporanReturPenjualan.Show()
    End Sub

    Private Sub PencapaianSalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PencapaianSalesToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmLaporanPencapaian.TopLevel = False
        panelForm.Controls.Add(frmLaporanPencapaian)
        frmLaporanPencapaian.Show()
    End Sub

    Private Sub BarangTerjualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BarangTerjualToolStripMenuItem.Click
        ' load form ke dalam panelForm
        frmLaporanBarang.TopLevel = False
        panelForm.Controls.Add(frmLaporanBarang)
        frmLaporanBarang.Show()
    End Sub
End Class