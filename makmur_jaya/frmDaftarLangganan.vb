﻿Imports System.Data.OleDb

Public Class frmDaftarLangganan
#Region "Sub"
    Sub clear()
        Call nilaiAwal()

        Me.txtTitle.Clear()
        Me.txtNama.Clear()
        Me.txtAlamat.Clear()
        Me.txtKota.Clear()
        Me.txtTelepon.Clear()
        Me.txtNPWP.Clear()
        Me.txtPlafondWaktu.Clear()
        Me.txtFakturPajak.Clear()
        Me.txtPajakStandar.Clear()

        Me.txtTitle.Focus()
    End Sub

    Sub nilaiAwal()
        Me.txtPlafondKredit.Text = 0
    End Sub

    Sub btnDefault()
        Me.btnSimpan.Enabled = True
        Me.btnUbah.Enabled = False
        Me.btnHapus.Enabled = False
        Me.btnBatal.Enabled = True
    End Sub

    Sub btnOnData()
        Me.btnSimpan.Enabled = False
        Me.btnUbah.Enabled = True
        Me.btnHapus.Enabled = True
        Me.btnBatal.Enabled = True
    End Sub

    Sub kodeLangganan()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_pelanggan WHERE id IN (SELECT MAX(id) FROM tb_pelanggan) ORDER BY id ASC", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            Dim no_urut As String
            Dim hitungan As Long

            If Not dr.HasRows Then
                no_urut = Format(Now, "yyyyMMdd") + "001"
            Else
                If Microsoft.VisualBasic.Left(dr.GetString(0), 8) <> Format(Now, "yyyyMMdd") Then
                    no_urut = Format(Now, "yyyyMMdd") + "001"
                Else
                    hitungan = dr.GetString(0) + 1
                    no_urut = Format(Now, "yyyyMMdd") + Microsoft.VisualBasic.Right("000" & hitungan, 3)
                End If
            End If

            ' DataReader Close
            dr.Close()

            Me.lblID.Text = no_urut
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Sub tampilData()
        Try
            da = New OleDbDataAdapter("SELECT * FROM tb_pelanggan ORDER BY id ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_pelanggan")
            dgvLangganan.DataSource = (ds.Tables("tb_pelanggan"))
            dgvLangganan.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

    Private Sub frmDaftarLangganan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
        Call nilaiAwal()
        Call tampilData()
        Call kodeLangganan()
        Call btnDefault()
    End Sub

#Region "Button"
    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Try
            If Me.txtTitle.Text = "" Or Me.txtNama.Text = "" Or Me.txtPlafondWaktu.Text = "" Or Me.txtPlafondKredit.Text = "" Then
                MsgBox("Harap masukan data yang dibutuhkan!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Dim cekData As String = "SELECT title_langganan FROM tb_pelanggan WHERE title_langganan = '" & UCase(Me.txtTitle.Text) & "'"
                cmd = New OleDbCommand(cekData, conn)
                dr = cmd.ExecuteReader
                dr.Read()

                If Not dr.HasRows Then
                    ' DataReader Close
                    dr.Close()

                    Dim simpan As String = "INSERT INTO tb_pelanggan (id, title_langganan, nama_langganan, alamat, kota, telepon, npwp, plafond_waktu, plafond_kredit, faktur_pajak, pajak_standar) " & _
                    "VALUES ('" & Me.lblID.Text & "', '" & UCase(Me.txtTitle.Text) & "', '" & UCase(Me.txtNama.Text) & "', '" & UCase(Me.txtAlamat.Text) & "', '" & UCase(Me.txtKota.Text) & "', '" & UCase(Me.txtTelepon.Text) & "', '" & Me.txtNPWP.Text & "', " & CInt(Me.txtPlafondWaktu.Text) & ", " & CDbl(Me.txtPlafondKredit.Text) & ", '" & UCase(Me.txtFakturPajak.Text) & "', '" & UCase(Me.txtPajakStandar.Text) & "')"
                    cmd = New OleDbCommand(simpan, conn)
                    cmd.ExecuteNonQuery()

                    MsgBox("Data berhasil disimpan!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodeLangganan()
                    Call btnDefault()
                    Call tampilData()
                Else
                    ' DataReader Close
                    dr.Close()

                    MsgBox("Data sudah ada, pastikan Title tidak sama!", MsgBoxStyle.Information, "Informasi")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnUbah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUbah.Click
        Try
            Dim ubah As String = "UPDATE tb_pelanggan SET " & _
                "title_langganan = '" & UCase(Me.txtTitle.Text) & "', " & _
                "nama_langganan = '" & UCase(Me.txtNama.Text) & "', " & _
                "alamat = '" & UCase(Me.txtAlamat.Text) & "', " & _
                "kota = '" & UCase(Me.txtKota.Text) & "', " & _
                "telepon = '" & UCase(Me.txtTelepon.Text) & "', " & _
                "npwp = '" & Me.txtNPWP.Text & "', " & _
                "plafond_waktu = '" & CInt(Me.txtPlafondWaktu.Text) & "', " & _
                "plafond_kredit = '" & CDbl(Me.txtPlafondKredit.Text) & "', " & _
                "faktur_pajak = '" & UCase(Me.txtFakturPajak.Text) & "', " & _
                "pajak_standar = '" & UCase(Me.txtPajakStandar.Text) & "' " & _
                "WHERE id = '" & Me.lblID.Text & "'"
            cmd = New OleDbCommand(ubah, conn)
            cmd.ExecuteNonQuery()

            MsgBox("Data berhasil diperbaharui!", MsgBoxStyle.Information, "Informasi")

            Call clear()
            Call kodeLangganan()
            Call btnDefault()
            Call tampilData()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        Try
            If MsgBox("Apakah Anda ingin menghapus data '" & UCase(Me.txtTitle.Text) & "'?", MsgBoxStyle.YesNo, "Informasi") = MsgBoxResult.Yes Then
                Dim cekData As String = "SELECT id FROM tb_pelanggan WHERE id = '" & Me.lblID.Text & "'"
                cmd = New OleDbCommand(cekData, conn)
                dr = cmd.ExecuteReader
                dr.Read()

                If dr.HasRows Then
                    ' DataReader Close
                    dr.Close()

                    Dim hapus As String = "DELETE FROM tb_pelanggan WHERE id = '" & Me.lblID.Text & "'"
                    cmd = New OleDbCommand(hapus, conn)
                    cmd.ExecuteNonQuery()

                    MsgBox("Data sudah dihapus!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodeLangganan()
                    Call btnDefault()
                    Call tampilData()
                Else
                    ' DataReader Close
                    dr.Close()

                    MsgBox("Maaf, data yang dimaksud tidak ada!", MsgBoxStyle.Information, "Informasi")

                    Call clear()
                    Call kodeLangganan()
                    Call btnDefault()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatal.Click
        Call clear()
        Call kodeLangganan()
        Call btnDefault()
    End Sub
#End Region

    Private Sub txtPencarian_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPencarian.TextChanged
        Try
            If Me.txtPencarian.Text = "" Then
                Call tampilData()
            Else
                Dim cari As String = "SELECT * FROM tb_pelanggan WHERE " & _
                    "title_langganan LIKE '%" & Me.txtPencarian.Text & "%' OR " & _
                    "nama_langganan LIKE '%" & Me.txtPencarian.Text & "%' ORDER BY id ASC"
                da = New OleDbDataAdapter(cari, conn)
                dt.Clear()
                da.Fill(dt)
                dgvLangganan.DataSource = dt
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub dgvLangganan_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLangganan.CellContentDoubleClick
        Try
            If IsDBNull(dgvLangganan.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                MsgBox("Tidak ada data yang dipilih!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Me.lblID.Text = dgvLangganan.Rows(e.RowIndex).Cells(0).Value
                Me.txtTitle.Text = dgvLangganan.Rows(e.RowIndex).Cells(1).Value
                Me.txtNama.Text = dgvLangganan.Rows(e.RowIndex).Cells(2).Value
                Me.txtAlamat.Text = dgvLangganan.Rows(e.RowIndex).Cells(3).Value
                Me.txtKota.Text = dgvLangganan.Rows(e.RowIndex).Cells(4).Value
                Me.txtTelepon.Text = dgvLangganan.Rows(e.RowIndex).Cells(5).Value
                Me.txtNPWP.Text = dgvLangganan.Rows(e.RowIndex).Cells(6).Value
                Me.txtPlafondWaktu.Text = dgvLangganan.Rows(e.RowIndex).Cells(7).Value
                Me.txtPlafondKredit.Text = FormatCurrency(dgvLangganan.Rows(e.RowIndex).Cells(8).Value)
                Me.txtFakturPajak.Text = dgvLangganan.Rows(e.RowIndex).Cells(9).Value
                Me.txtPajakStandar.Text = dgvLangganan.Rows(e.RowIndex).Cells(10).Value

                Call btnOnData()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

#Region "Identifier_Number"
    Private Sub txtTelepon_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelepon.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtPlafondWaktu_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPlafondWaktu.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub
#End Region

#Region "Format_Currency"
    Private Sub txtPlafondKredit_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPlafondKredit.Leave
        If Me.txtPlafondKredit.Text = "" Or Not IsNumeric(Me.txtPlafondKredit.Text) Then
            Exit Sub
        End If

        Me.txtPlafondKredit.Text = FormatCurrency(CDbl(Me.txtPlafondKredit.Text))
    End Sub
#End Region
End Class