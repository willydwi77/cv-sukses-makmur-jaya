﻿Imports System.Data.OleDb

Public Class frmDaftarBarang

#Region "Sub"
    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Call nilaiAwal()
        Me.txtBarang.Clear()
        Me.txtGrup.Clear()
        Me.cmbPabrik.Text = ""
        Me.txtSatBesar.Clear()
        Me.txtSatTanggung.Clear()
        Me.txtSatKecil.Clear()
        Me.txtBarang.Focus()
    End Sub

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub nilaiAwal()
        Me.txtIsiBesar.Text = 0
        Me.txtIsiTanggung.Text = 0
        Me.txtStockAwal.Text = 0
        Me.txtMinimStock.Text = 0
        Me.txtMaksStock.Text = 0
        Me.txtHargaBeliBesar.Text = 0
        Me.txtHargaBeliTanggung.Text = 0
        Me.txtHargaBeliKecil.Text = 0
        Me.txtJualBesar.Text = 0
        Me.txtJualTanggung.Text = 0
        Me.txtJualKecil.Text = 0
    End Sub

    ' nilai boolean untuk button saat pertama kali form load
    Sub btnDefault()
        Me.btnSimpan.Enabled = True
        Me.btnUbah.Enabled = False
        Me.btnHapus.Enabled = False
        Me.btnBatal.Enabled = True
    End Sub

    ' nilai boolean untuk button saat input
    Sub btnOnData()
        Me.btnSimpan.Enabled = False
        Me.btnUbah.Enabled = True
        Me.btnHapus.Enabled = True
        Me.btnBatal.Enabled = True
    End Sub

    ' generate otomatis untuk kode barang
    Sub kodeBarang()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_barang WHERE id IN (SELECT MAX(id) FROM tb_barang) ORDER BY id ASC", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            Dim no_urut As String
            Dim hitungan As Long

            If Not dr.HasRows Then
                no_urut = "0001"
            Else
                hitungan = dr.GetValue(0) + 1
                no_urut = Format(hitungan, "0000")
            End If

            ' DataReader Close
            dr.Close()

            Me.lblID.Text = no_urut
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' mengambil data dari tabel barang
    Sub tampilData()
        Try
            da = New OleDbDataAdapter("SELECT brg.id, brg.nama_barang, brg.grup, pb.nama_pabrik, brg.satuan_besar, brg.isi_besar, " & _
                "brg.satuan_tanggung, brg.isi_tanggung, brg.satuan_kecil, brg.stock_awal, brg.minimum_stock, brg.maksimum_stock, " & _
                "brg.harga_beli_besar, brg.harga_beli_tanggung, brg.harga_beli_kecil, brg.harga_jual_besar, " & _
                "brg.harga_jual_tanggung, brg.harga_jual_kecil " & _
                "FROM tb_barang AS brg INNER JOIN tb_pabrik AS pb ON brg.id_pabrik = pb.id ORDER BY brg.id ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_barang")
            dgvBarang.DataSource = (ds.Tables("tb_barang"))
            dgvBarang.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' mengambil data id dari tabel pabrik
    Sub ambilPabrik()
        Try
            cmd = New OleDbCommand("SELECT nama_pabrik FROM tb_pabrik ORDER BY nama_pabrik ASC", conn)
            dr = cmd.ExecuteReader

            While dr.Read
                Me.cmbPabrik.Items.Add(dr.GetValue(0))
            End While

            ' DataReader Close
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' autocomplete untuk grup
    Sub autocompleteGrup()
        Try
            da = New OleDbDataAdapter("SELECT grup FROM tb_barang ORDER BY grup ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_barang")

            Dim col As New AutoCompleteStringCollection

            For Each row In ds.Tables(0).Rows
                col.Add(row(0).ToString)
            Next

            Me.txtGrup.AutoCompleteSource = AutoCompleteSource.CustomSource
            Me.txtGrup.AutoCompleteCustomSource = col
            Me.txtGrup.AutoCompleteMode = AutoCompleteMode.Suggest
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' autocomplete untuk satuan besar
    Sub autocompleteSatuanBesar()
        Try
            da = New OleDbDataAdapter("SELECT satuan_besar FROM tb_barang ORDER BY satuan_besar ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_barang")

            Dim col As New AutoCompleteStringCollection

            For Each row In ds.Tables(0).Rows
                col.Add(row(0).ToString)
            Next

            Me.txtSatBesar.AutoCompleteSource = AutoCompleteSource.CustomSource
            Me.txtSatBesar.AutoCompleteCustomSource = col
            Me.txtSatBesar.AutoCompleteMode = AutoCompleteMode.Suggest
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' autocomplete untuk satuan tanggung
    Sub autocompleteSatuanTanggung()
        Try
            da = New OleDbDataAdapter("SELECT satuan_tanggung FROM tb_barang ORDER BY satuan_tanggung ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_barang")

            Dim col As New AutoCompleteStringCollection

            For Each row In ds.Tables(0).Rows
                col.Add(row(0).ToString)
            Next

            Me.txtSatTanggung.AutoCompleteSource = AutoCompleteSource.CustomSource
            Me.txtSatTanggung.AutoCompleteCustomSource = col
            Me.txtSatTanggung.AutoCompleteMode = AutoCompleteMode.Suggest
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' autocomplete untuk satuan kecil
    Sub autocompleteSatuanKecil()
        Try
            da = New OleDbDataAdapter("SELECT satuan_kecil FROM tb_barang ORDER BY satuan_kecil ASC", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_barang")

            Dim col As New AutoCompleteStringCollection

            For Each row In ds.Tables(0).Rows
                col.Add(row(0).ToString)
            Next

            Me.txtSatKecil.AutoCompleteSource = AutoCompleteSource.CustomSource
            Me.txtSatKecil.AutoCompleteCustomSource = col
            Me.txtSatKecil.AutoCompleteMode = AutoCompleteMode.Suggest
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

    ' main load
    Private Sub frmDaftarBarang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call nilaiAwal()
        Call kodeBarang()
        Call autocompleteGrup()
        Call ambilPabrik()
        Call autocompleteSatuanBesar()
        Call autocompleteSatuanTanggung()
        Call autocompleteSatuanKecil()
        Call tampilData()
        Call btnDefault()
    End Sub

#Region "TextBox"
    Private Sub txtPencarian_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPencarian.TextChanged
        Try
            If Me.txtPencarian.Text = "" Then
                Call tampilData()
            Else
                Dim cari As String = "SELECT * FROM tb_barang WHERE " & _
                    "nama_barang LIKE '%" & Me.txtPencarian.Text & "%' OR " & _
                    "grup LIKE '%" & Me.txtPencarian.Text & "%' OR " & _
                    "satuan_besar LIKE '%" & Me.txtPencarian.Text & "%' OR " & _
                    "satuan_tanggung LIKE '%" & Me.txtPencarian.Text & "%' ORDER BY id ASC"
                da = New OleDbDataAdapter(cari, conn)
                dt.Clear()
                da.Fill(dt)
                dgvBarang.DataSource = dt
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

#Region "Button"
    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Try
            If Me.txtBarang.Text = "" Then
                MsgBox("Harap masukan data yang dibutuhkan!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                cmd = New OleDbCommand("SELECT nama_barang FROM tb_barang WHERE nama_barang = ?", conn)
                cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.txtBarang.Text)

                dr = cmd.ExecuteReader
                dr.Read()

                If Not dr.HasRows Then
                    ' menutup sesi dr.Read()
                    dr.Close()

                    Dim simpan As String = "INSERT INTO tb_barang ([id], [nama_barang], [grup], [id_pabrik], [satuan_besar], [isi_besar], [satuan_tanggung], " & _
                        "[isi_tanggung], [satuan_kecil], [stock_awal], [minimum_stock], [maksimum_stock], [harga_beli_besar], [harga_beli_tanggung], " & _
                        "[harga_beli_kecil], [harga_jual_besar], [harga_jual_tanggung], [harga_jual_kecil]) " & _
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                    cmd = New OleDbCommand(simpan, conn)

                    cmd.Parameters.Add("@id", OleDbType.VarChar).Value = Me.lblID.Text
                    cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.txtBarang.Text)
                    cmd.Parameters.Add("@grup", OleDbType.VarChar).Value = UCase(Me.txtGrup.Text)
                    cmd.Parameters.Add("@id_pabrik", OleDbType.VarChar).Value = UCase(Me.panelIDPabrik.Text)
                    cmd.Parameters.Add("@satuan_besar", OleDbType.VarChar).Value = UCase(Me.txtSatBesar.Text)
                    cmd.Parameters.Add("@isi_besar", OleDbType.Double).Value = CDbl(Me.txtIsiBesar.Text)
                    cmd.Parameters.Add("@satuan_tanggung", OleDbType.VarChar).Value = UCase(Me.txtSatTanggung.Text)
                    cmd.Parameters.Add("@isi_tanggung", OleDbType.Double).Value = CDbl(Me.txtIsiTanggung.Text)
                    cmd.Parameters.Add("@satuan_kecil", OleDbType.VarChar).Value = UCase(Me.txtSatKecil.Text)
                    cmd.Parameters.Add("@stock_awal", OleDbType.Double).Value = CDbl(Me.txtStockAwal.Text)
                    cmd.Parameters.Add("@minimum_stock", OleDbType.Double).Value = CDbl(Me.txtMinimStock.Text)
                    cmd.Parameters.Add("@maksimum_stock", OleDbType.Double).Value = CDbl(Me.txtMaksStock.Text)
                    cmd.Parameters.Add("@harga_beli_besar", OleDbType.Double).Value = CDbl(Me.txtHargaBeliBesar.Text)
                    cmd.Parameters.Add("@harga_beli_tanggung", OleDbType.VarChar).Value = CDbl(Me.txtHargaBeliTanggung.Text)
                    cmd.Parameters.Add("@harga_beli_kecil", OleDbType.Double).Value = CDbl(Me.txtHargaBeliKecil.Text)
                    cmd.Parameters.Add("@harga_jual_besar", OleDbType.Double).Value = CDbl(Me.txtJualBesar.Text)
                    cmd.Parameters.Add("@harga_jual_tanggung", OleDbType.Double).Value = CDbl(Me.txtJualTanggung.Text)
                    cmd.Parameters.Add("@harga_jual_kecil", OleDbType.VarChar).Value = CDbl(Me.txtJualKecil.Text)

                    cmd.CommandText = simpan
                    cmd.ExecuteNonQuery()

                    MsgBox("Data berhasil disimpan!", MsgBoxStyle.Information, "Informasi")

                    Call bersih()
                    Call kodeBarang()
                    Call autocompleteGrup()
                    Call ambilPabrik()
                    Call autocompleteSatuanBesar()
                    Call autocompleteSatuanTanggung()
                    Call autocompleteSatuanKecil()
                    Call btnDefault()
                    Call tampilData()
                Else
                    ' menutup sesi dr.Read()
                    dr.Close()

                    MsgBox("Data sudah ada, pastikan Nama Barang tidak sama!", MsgBoxStyle.Information, "Informasi")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnUbah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUbah.Click
        Try
            Dim ubah As String = "UPDATE tb_barang SET " & _
                "[nama_barang] = ?, [grup] = ?, [id_pabrik] = ?, [satuan_besar] = ?, [isi_besar] = ?, [satuan_tanggung] = ?, [isi_tanggung] = ?, [satuan_kecil] = ?, " & _
                "[stock_awal] = ?, [minimum_stock] = ?, [maksimum_stock] = ?, [harga_beli_besar] = ?, [harga_beli_tanggung] = ?, [harga_beli_kecil] = ?, " & _
                "[harga_jual_besar] = ?, [harga_jual_tanggung] = ?, [harga_jual_kecil] = ? " & _
                "WHERE [id] = ?"
            cmd = New OleDbCommand(ubah, conn)

            cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.txtBarang.Text)
            cmd.Parameters.Add("@grup", OleDbType.VarChar).Value = UCase(Me.txtGrup.Text)
            cmd.Parameters.Add("@id_pabrik", OleDbType.VarChar).Value = UCase(Me.panelIDPabrik.Text)
            cmd.Parameters.Add("@satuan_besar", OleDbType.VarChar).Value = UCase(Me.txtSatBesar.Text)
            cmd.Parameters.Add("@isi_besar", OleDbType.Double).Value = CDbl(Me.txtIsiBesar.Text)
            cmd.Parameters.Add("@satuan_tanggung", OleDbType.VarChar).Value = UCase(Me.txtSatTanggung.Text)
            cmd.Parameters.Add("@isi_tanggung", OleDbType.Double).Value = CDbl(Me.txtIsiTanggung.Text)
            cmd.Parameters.Add("@satuan_kecil", OleDbType.VarChar).Value = UCase(Me.txtSatKecil.Text)
            cmd.Parameters.Add("@stock_awal", OleDbType.Double).Value = CDbl(Me.txtStockAwal.Text)
            cmd.Parameters.Add("@minimum_stock", OleDbType.Double).Value = CDbl(Me.txtMinimStock.Text)
            cmd.Parameters.Add("@maksimum_stock", OleDbType.Double).Value = CDbl(Me.txtMaksStock.Text)
            cmd.Parameters.Add("@harga_beli_besar", OleDbType.Double).Value = CDbl(Me.txtHargaBeliBesar.Text)
            cmd.Parameters.Add("@harga_beli_tanggung", OleDbType.VarChar).Value = CDbl(Me.txtHargaBeliTanggung.Text)
            cmd.Parameters.Add("@harga_beli_kecil", OleDbType.Double).Value = CDbl(Me.txtHargaBeliKecil.Text)
            cmd.Parameters.Add("@harga_jual_besar", OleDbType.Double).Value = CDbl(Me.txtJualBesar.Text)
            cmd.Parameters.Add("@harga_jual_tanggung", OleDbType.Double).Value = CDbl(Me.txtJualTanggung.Text)
            cmd.Parameters.Add("@harga_jual_kecil", OleDbType.VarChar).Value = CDbl(Me.txtJualKecil.Text)
            cmd.Parameters.Add("@id", OleDbType.VarChar).Value = Me.lblID.Text

            cmd.CommandText = ubah
            cmd.ExecuteNonQuery()

            MsgBox("Data berhasil diperbaharui!", MsgBoxStyle.Information, "Informasi")

            Call bersih()
            Call kodeBarang()
            Call autocompleteGrup()
            Call ambilPabrik()
            Call autocompleteSatuanBesar()
            Call autocompleteSatuanTanggung()
            Call autocompleteSatuanKecil()
            Call btnDefault()
            Call tampilData()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        Try
            If MsgBox("Apakah Anda ingin menghapus data '" & UCase(Me.txtBarang.Text) & "'?", MsgBoxStyle.YesNo, "Informasi") = MsgBoxResult.Yes Then
                cmd = New OleDbCommand("SELECT id FROM tb_barang WHERE id = ?", conn)
                cmd.Parameters.Add("@id", OleDbType.VarChar).Value = Me.lblID.Text

                dr = cmd.ExecuteReader
                dr.Read()

                If dr.HasRows Then
                    ' menutup sesi dr.Read()
                    dr.Close()

                    Dim hapus As String = "DELETE FROM tb_barang WHERE id = ?"
                    cmd = New OleDbCommand(hapus, conn)
                    cmd.Parameters.Add("@id", OleDbType.VarChar).Value = Me.lblID.Text

                    cmd.CommandText = hapus
                    cmd.ExecuteNonQuery()

                    MsgBox("Data sudah dihapus!", MsgBoxStyle.Information, "Informasi")

                    Call bersih()
                    Call kodeBarang()
                    Call autocompleteGrup()
                    Call ambilPabrik()
                    Call autocompleteSatuanBesar()
                    Call autocompleteSatuanTanggung()
                    Call autocompleteSatuanKecil()
                    Call btnDefault()
                    Call tampilData()
                Else
                    ' menutup sesi dr.Read()
                    dr.Close()

                    MsgBox("Maaf, data yang dimaksud tidak ada!", MsgBoxStyle.Information, "Informasi")

                    Call bersih()
                    Call kodeBarang()
                    Call autocompleteGrup()
                    Call ambilPabrik()
                    Call autocompleteSatuanBesar()
                    Call autocompleteSatuanTanggung()
                    Call autocompleteSatuanKecil()
                    Call btnDefault()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatal.Click
        Call bersih()
        Call kodeBarang()
        Call autocompleteGrup()
        Call ambilPabrik()
        Call autocompleteSatuanBesar()
        Call autocompleteSatuanTanggung()
        Call autocompleteSatuanKecil()
        Call btnDefault()
    End Sub

    Private Sub btnPabrik_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPabrik.Click
        frmDaftarPabrik.TopLevel = False
        frmMain.panelForm.Controls.Add(frmDaftarPabrik)
        frmDaftarPabrik.BringToFront()
        frmDaftarPabrik.Show()
    End Sub
#End Region

#Region "ComboBox"
    Private Sub cmbPabrik_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPabrik.SelectedIndexChanged
        Try
            ' mengambil ID nama pabrik
            cmd = New OleDbCommand("SELECT id FROM tb_pabrik WHERE nama_pabrik = ?", conn)
            cmd.Parameters.Add("@nama_pabrik", OleDbType.VarChar).Value = UCase(Me.cmbPabrik.Text)

            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDPabrik.Text = dr.GetValue(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

#Region "Number"
    Private Sub txtIsiBesar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIsiBesar.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtIsiTanggung_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIsiTanggung.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtStockAwal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStockAwal.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtNilaiAwal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtMinimStock_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMinimStock.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtMaksStock_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMaksStock.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtHargaModal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtHargaBeliBesar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHargaBeliBesar.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtHargaBeliTanggung_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHargaBeliTanggung.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtHargaBeliKecil_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHargaBeliKecil.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtJualBesar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtJualBesar.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtJualTanggung_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtJualTanggung.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtJualKecil_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtJualKecil.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub txtJualGrosir_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub
#End Region

#Region "Format Currency"
    Private Sub txtHargaBeliBesar_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaBeliBesar.Leave
        If Me.txtHargaBeliBesar.Text = "" Or Not IsNumeric(Me.txtHargaBeliBesar.Text) Then
            Exit Sub
        End If

        Me.txtHargaBeliBesar.Text = FormatCurrency(CDbl(Me.txtHargaBeliBesar.Text))
    End Sub

    Private Sub txtHargaBeliTanggung_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaBeliTanggung.Leave
        If Me.txtHargaBeliTanggung.Text = "" Or Not IsNumeric(Me.txtHargaBeliTanggung.Text) Then
            Exit Sub
        End If

        Me.txtHargaBeliTanggung.Text = FormatCurrency(CDbl(Me.txtHargaBeliTanggung.Text))
    End Sub

    Private Sub txtHargaBeliKecil_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaBeliKecil.Leave
        If Me.txtHargaBeliKecil.Text = "" Or Not IsNumeric(Me.txtHargaBeliKecil.Text) Then
            Exit Sub
        End If

        Me.txtHargaBeliKecil.Text = FormatCurrency(CDbl(Me.txtHargaBeliKecil.Text))
    End Sub

    Private Sub txtJualBesar_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJualBesar.Leave
        If Me.txtJualBesar.Text = "" Or Not IsNumeric(Me.txtJualBesar.Text) Then
            Exit Sub
        End If

        Me.txtJualBesar.Text = FormatCurrency(CDbl(Me.txtJualBesar.Text))
    End Sub

    Private Sub txtJualTanggung_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJualTanggung.Leave
        If Me.txtJualTanggung.Text = "" Or Not IsNumeric(Me.txtJualTanggung.Text) Then
            Exit Sub
        End If

        Me.txtJualTanggung.Text = FormatCurrency(CDbl(Me.txtJualTanggung.Text))
    End Sub

    Private Sub txtJualKecil_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJualKecil.Leave
        If Me.txtJualKecil.Text = "" Or Not IsNumeric(Me.txtJualKecil.Text) Then
            Exit Sub
        End If

        Me.txtJualKecil.Text = FormatCurrency(CDbl(Me.txtJualKecil.Text))
    End Sub
#End Region

    Private Sub dgvBarang_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBarang.CellContentDoubleClick
        Try
            If IsDBNull(dgvBarang.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                MsgBox("Tidak ada data yang dipilih!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Me.lblID.Text = dgvBarang.Rows(e.RowIndex).Cells(0).Value
                Me.txtBarang.Text = dgvBarang.Rows(e.RowIndex).Cells(1).Value
                Me.txtGrup.Text = dgvBarang.Rows(e.RowIndex).Cells(2).Value
                Me.cmbPabrik.Text = dgvBarang.Rows(e.RowIndex).Cells(3).Value
                Me.txtSatBesar.Text = dgvBarang.Rows(e.RowIndex).Cells(4).Value
                Me.txtIsiBesar.Text = CDbl(dgvBarang.Rows(e.RowIndex).Cells(5).Value)
                Me.txtSatTanggung.Text = dgvBarang.Rows(e.RowIndex).Cells(6).Value
                Me.txtIsiTanggung.Text = CDbl(dgvBarang.Rows(e.RowIndex).Cells(7).Value)
                Me.txtSatKecil.Text = dgvBarang.Rows(e.RowIndex).Cells(8).Value
                Me.txtStockAwal.Text = CDbl(dgvBarang.Rows(e.RowIndex).Cells(9).Value)
                Me.txtMinimStock.Text = CDbl(dgvBarang.Rows(e.RowIndex).Cells(10).Value)
                Me.txtMaksStock.Text = CDbl(dgvBarang.Rows(e.RowIndex).Cells(11).Value)
                Me.txtHargaBeliBesar.Text = FormatCurrency(CDbl(dgvBarang.Rows(e.RowIndex).Cells(12).Value))
                Me.txtHargaBeliTanggung.Text = FormatCurrency(CDbl(dgvBarang.Rows(e.RowIndex).Cells(13).Value))
                Me.txtHargaBeliKecil.Text = FormatCurrency(CDbl(dgvBarang.Rows(e.RowIndex).Cells(14).Value))
                Me.txtJualBesar.Text = FormatCurrency(CDbl(dgvBarang.Rows(e.RowIndex).Cells(15).Value))
                Me.txtJualTanggung.Text = FormatCurrency(CDbl(dgvBarang.Rows(e.RowIndex).Cells(16).Value))
                Me.txtJualKecil.Text = FormatCurrency(CDbl(dgvBarang.Rows(e.RowIndex).Cells(17).Value))

                cmd = New OleDbCommand("SELECT id FROM tb_pabrik WHERE nama_pabrik = ?", conn)
                cmd.Parameters.Add("@nama_pabrik", OleDbType.VarChar).Value = UCase(Me.cmbPabrik.Text)

                dr = cmd.ExecuteReader
                dr.Read()

                If dr.HasRows Then
                    Me.panelIDPabrik.Text = dr.GetValue(0)
                End If

                Call btnOnData()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class