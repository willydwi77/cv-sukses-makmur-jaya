﻿Imports System.Data.OleDb

Public Class frmLaporanReturPembelian

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Me.cmbNoRetur.Text = ""
    End Sub

    ' Sub untuk mengambil data dari tabel retur pembelian
    ' dengan menambahkan data dari tabel ke dalam cmbNoRetur
    Sub ambilDataReturPembelian()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_retur_pembelian ORDER BY id DESC", conn)
            dr = cmd.ExecuteReader

            Do While dr.Read
                cmbNoRetur.Items.Add(dr.GetValue(0))
            Loop
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub frmLaporanReturPembelian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call ambilDataReturPembelian()
    End Sub

    Private Sub btnTampil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTampil.Click
        Try
            ' mengambil data CrystalReportViewer berdasarkan SelectionFormula dengan dua kondisi
            ' kondisi (1) jika cmbNoRetur TIDAK kosong atau ""
            ' kondisi (2) default ketika semua ComboBox dalam keadaan kosong atau ""
            If Not Me.cmbNoRetur.Text = "" Then
                crvReturPembelian.SelectionFormula = "{tb_retur_pembelian.id} = '" & Me.cmbNoRetur.Text & "' AND " & _
                    "{tb_retur_pembelian.tanggal_retur} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_retur_pembelian.tanggal_retur} <= CDate('" & Me.dtpAkhir.Value & "')"
            Else
                crvReturPembelian.SelectionFormula = "{tb_retur_pembelian.tanggal_retur} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_retur_pembelian.tanggal_retur} <= CDate('" & Me.dtpAkhir.Value & "')"
            End If

            ' load file CrystalReport
            cryRPT.Load("crReturPembelian.rpt")

            Call konfig_cr()

            ' tampilkan ke dalam CrystalReportViewer
            crvReturPembelian.ReportSource = cryRPT
            crvReturPembelian.RefreshReport()

            Call bersih()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class