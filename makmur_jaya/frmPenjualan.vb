﻿Imports System.Data.OleDb

Public Class frmPenjualan

#Region "Sub"
    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Me.cmbBarang.Text = ""
        Me.cmbSalesman.Text = ""
        Me.cmbLangganan.Text = ""
        Me.txtAlamatLangganan.Clear()
        Me.txtQTY.Text = 0
        Me.txtBonus.Text = 0
        Me.txtSat.Clear()
        Me.txtHargaSat.Text = 0
        Me.txtDis.Text = 0
        Me.txtJumlah.Text = 0
        Me.txtRp.Text = 0
        Me.txtPPN.Text = 0
        Me.txtHasilPPN.Text = 0
        Me.txtTotalRp.Text = 0
        Me.cmbBarang.Focus()
    End Sub

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub listDefault()
        Me.cmbBarang.Text = ""
        Me.txtQTY.Text = 0
        Me.txtBonus.Text = 0
        Me.txtSat.Clear()
        Me.txtHargaSat.Text = 0
        Me.txtDis.Text = 0
        Me.txtJumlah.Text = 0
        Me.txtHasilPPN.Text = 0
        Me.txtTotalRp.Text = 0
        Me.cmbBarang.Focus()
    End Sub

    ' nilai boolean untuk button saat pertama kali form load
    Sub btnDefault()
        Me.btnTambah.Enabled = True
        Me.btnSimpan.Enabled = False
        Me.btnHapus.Enabled = False
        Me.btnBatal.Enabled = False
    End Sub

    ' nilai boolean untuk button saat input
    Sub btnOn()
        Me.btnTambah.Enabled = False
        Me.btnSimpan.Enabled = True
        Me.btnHapus.Enabled = True
        Me.btnBatal.Enabled = True
    End Sub

    ' membuat kolom untuk DataGridView
    Sub dgvColumns()
        ' columns
        With dgvPenjualan
            .Columns.Add("ID", "ID")
            .Columns.Add("NAMA BARANG", "NAMA BARANG")
            .Columns.Add("QTY", "QTY")
            .Columns.Add("BONUS", "BONUS")
            .Columns.Add("SATUAN", "SATUAN")
            .Columns.Add("HARGA SATUAN", "HARGA SATUAN")
            .Columns.Add("DISKON", "DISKON")
            .Columns.Add("JUMLAH", "JUMLAH")
            .Columns.Add("STOCK AKHIR", "STOCK AKHIR")
        End With
    End Sub

    ' menghitung jumlah qty dengan harga satuan
    Function hitungJumlah(ByVal qty As Double, ByVal hargaSatuan As Double, ByVal diskon As Double) As Double
        Dim harga_awal, harga_akhir, jumlah As Double

        harga_awal = qty * hargaSatuan
        harga_akhir = (diskon / 100) * harga_awal
        jumlah = harga_awal - harga_akhir

        Return jumlah
    End Function

    ' menghitung total jumlah
    Function hitungRp() As Double
        Dim rp As Double

        For Each row As DataGridViewRow In dgvPenjualan.Rows
            rp += row.Cells("JUMLAH").Value
        Next

        Return rp
    End Function

    ' generate otomatis untuk noFaktur / noRetur
    Sub noFaktur()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_penjualan WHERE id IN (SELECT MAX(id) FROM tb_penjualan) ORDER BY id ASC", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            Dim no_urut As String
            Dim hitungan As Long

            If Not dr.HasRows Then
                no_urut = Format(Me.dtpFaktur.Value, "yyyyMMdd") + "001"
            Else
                If Microsoft.VisualBasic.Left(dr.GetString(0), 8) <> Format(Me.dtpFaktur.Value, "yyyyMMdd") Then
                    no_urut = Format(Me.dtpFaktur.Value, "yyyyMMdd") + "001"
                Else
                    hitungan = dr.GetString(0) + 1
                    no_urut = Format(Me.dtpFaktur.Value, "yyyyMMdd") + Microsoft.VisualBasic.Right("000" & hitungan, 3)
                End If
            End If

            ' menutup sesi dr.Read()
            dr.Close()

            Me.lblFaktur.Text = no_urut
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' mengambil data id dari tabel salesman
    Sub ambilSalesman()
        Try
            cmd = New OleDbCommand("SELECT nama_karyawan FROM tb_karyawan ORDER BY nama_karyawan ASC", conn)
            dr = cmd.ExecuteReader

            While dr.Read
                Me.cmbSalesman.Items.Add(dr.GetValue(0))
            End While

            ' menutup sesi dr.Read()
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' mengambil data id dari tabel langganan
    Sub ambilLangganan()
        Try
            cmd = New OleDbCommand("SELECT nama_langganan FROM tb_pelanggan ORDER BY nama_langganan ASC", conn)
            dr = cmd.ExecuteReader

            While dr.Read
                Me.cmbLangganan.Items.Add(dr.GetValue(0))
            End While

            ' menutup sesi dr.Read()
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' mengambil data id dari tabel barang
    Sub ambilBarang()
        Try
            cmd = New OleDbCommand("SELECT nama_barang FROM tb_barang ORDER BY nama_barang ASC", conn)
            dr = cmd.ExecuteReader

            While dr.Read
                Me.cmbBarang.Items.Add(dr.GetValue(0))
            End While

            ' menutup sesi dr.Read()
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

    ' main load
    Private Sub frmPenjualan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call bersih()
        Call btnDefault()
        Call noFaktur()
        Call ambilSalesman()
        Call ambilLangganan()
        Call ambilBarang()
        Call dgvColumns()
    End Sub

#Region "TextBox"
    Private Sub dtpFaktur_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFaktur.ValueChanged
        Call noFaktur()
    End Sub

    Private Sub txtQTY_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQTY.TextChanged
        Try
            ' cek jika nilai sama dengan "" atau bukan angka maka abaikan
            If Me.txtQTY.Text = "" Or Not IsNumeric(Me.txtQTY.Text) Then
                Exit Sub
            End If

            If Me.txtHargaSat.Text = "" Or Not IsNumeric(Me.txtHargaSat.Text) Then
                Exit Sub
            End If

            If Me.txtDis.Text = "" Or Not IsNumeric(Me.txtDis.Text) Then
                Exit Sub
            End If

            ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
            Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub txtSat_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSat.TextChanged
        Try
            ' cek jika txtSat sama dengan
            If Me.txtSat.Text = UCase("KRT") Then ' satuan_besar
                cmd = New OleDbCommand("SELECT harga_jual_besar FROM tb_barang WHERE nama_barang = ? AND satuan_besar = ?", conn)
                cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.cmbBarang.Text)
                cmd.Parameters.Add("@satuan_besar", OleDbType.VarChar).Value = UCase(Me.txtSat.Text)

                dr = cmd.ExecuteReader
                dr.Read()

                Me.txtHargaSat.Text = FormatCurrency(dr.GetValue(0))

                ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
                Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
            ElseIf Me.txtSat.Text = UCase("LSN") Or Me.txtSat.Text = UCase("BAL") Then ' satuan_tanggung
                cmd = New OleDbCommand("SELECT harga_jual_tanggung FROM tb_barang WHERE nama_barang = ? AND satuan_tanggung = ?", conn)
                cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.cmbBarang.Text)
                cmd.Parameters.Add("@satuan_tanggung", OleDbType.VarChar).Value = UCase(Me.txtSat.Text)

                dr = cmd.ExecuteReader
                dr.Read()

                Me.txtHargaSat.Text = FormatCurrency(dr.GetValue(0))

                ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
                Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
            ElseIf Me.txtSat.Text = UCase("FLS") Or Me.txtSat.Text = UCase("BOX") Or Me.txtSat.Text = UCase("PCS") Then  ' satuan_kecil
                cmd = New OleDbCommand("SELECT harga_jual_kecil FROM tb_barang WHERE nama_barang = ? AND satuan_kecil = ?", conn)
                cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.cmbBarang.Text)
                cmd.Parameters.Add("@satuan_kecil", OleDbType.VarChar).Value = UCase(Me.txtSat.Text)

                dr = cmd.ExecuteReader
                dr.Read()

                Me.txtHargaSat.Text = FormatCurrency(dr.GetValue(0))

                ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
                Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub txtDis_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDis.TextChanged
        Try
            ' cek jika nilai sama dengan "" atau bukan angka maka abaikan
            If Me.txtQTY.Text = "" Or Not IsNumeric(Me.txtQTY.Text) Then
                Exit Sub
            End If

            If Me.txtHargaSat.Text = "" Or Not IsNumeric(Me.txtHargaSat.Text) Then
                Exit Sub
            End If

            If Me.txtDis.Text = "" Or Not IsNumeric(Me.txtDis.Text) Then
                Exit Sub
            End If

            ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
            Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "informasi")
        End Try
    End Sub

    Private Sub txtPPN_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPPN.TextChanged
        Try
            ' cek jika nilai sama dengan "" atau bukan angka maka abaikan
            If Me.txtRp.Text = "" Or Not IsNumeric(Me.txtRp.Text) Then
                Exit Sub
            End If

            If Me.txtPPN.Text = "" Or Not IsNumeric(Me.txtPPN.Text) Then
                Exit Sub
            End If

            ' rumus Hasil PPN dan Total RP
            Me.txtHasilPPN.Text = FormatCurrency((CDbl(Me.txtPPN.Text) / 100) * CDbl(Me.txtRp.Text))
            Me.txtTotalRp.Text = FormatCurrency(CDbl(Me.txtRp.Text) + CDbl(Me.txtHasilPPN.Text))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

#Region "ComboBox"
    Private Sub cmbBarang_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBarang.SelectedIndexChanged
        Try
            ' mengambil ID barang
            cmd = New OleDbCommand("SELECT id, stock_awal FROM tb_barang WHERE nama_barang = ?", conn)
            cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.cmbBarang.Text)

            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDBarang.Text = dr.GetValue(0)
                Me.panelStockAwal.Text = dr.GetValue(1)
            End If

            ' autocomplete satuan
            da = New OleDbDataAdapter("SELECT satuan_besar, satuan_tanggung, satuan_kecil FROM tb_barang WHERE nama_barang = '" & UCase(Me.cmbBarang.Text) & "' ", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_barang")

            Dim col As New AutoCompleteStringCollection

            For Each row In ds.Tables(0).Rows
                col.Add(row(0).ToString)
                col.Add(row(1).ToString)
                col.Add(row(2).ToString)
            Next

            Me.txtSat.AutoCompleteCustomSource = col

            Call btnOn()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub cmbSalesman_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalesman.SelectedIndexChanged
        Try
            ' mengambil ID salesman
            cmd = New OleDbCommand("SELECT id FROM tb_karyawan WHERE nama_karyawan = ?", conn)
            cmd.Parameters.Add("@nama_karyawan", OleDbType.VarChar).Value = UCase(Me.cmbSalesman.Text)

            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDSalesman.Text = dr.GetValue(0)
            End If

            Call btnOn()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub cmbLangganan_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLangganan.SelectedIndexChanged
        Try
            ' mengambil ID langganan
            cmd = New OleDbCommand("SELECT id, alamat FROM tb_pelanggan WHERE nama_langganan = ?", conn)
            cmd.Parameters.Add("@nama_langganan", OleDbType.VarChar).Value = UCase(Me.cmbLangganan.Text)

            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDLangganan.Text = dr.GetValue(0)
                Me.txtAlamatLangganan.Text = dr.GetValue(1)
            End If

            Call btnOn()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

#Region "Button"
    Private Sub btnTambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTambah.Click
        Call bersih()
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Try
            ' menyimpan data ke dalam table penjualan
            Dim simpanPenjualan As String = "INSERT INTO tb_penjualan ([id], [tanggal_penjualan], [id_karyawan], [id_langganan], [rp], [ppn], [harga_ppn], [total_rp]) " & _
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
            cmd = New OleDbCommand(simpanPenjualan, conn)

            cmd.Parameters.Add("@id", OleDbType.VarChar).Value = Me.lblFaktur.Text
            cmd.Parameters.Add("@tanggal_penjualan", OleDbType.Date).Value = Me.dtpFaktur.Value
            cmd.Parameters.Add("@id_karyawan", OleDbType.VarChar).Value = Me.panelIDSalesman.Text
            cmd.Parameters.Add("@id_langganan", OleDbType.VarChar).Value = Me.panelIDLangganan.Text
            cmd.Parameters.Add("@rp", OleDbType.Double).Value = CDbl(Me.txtRp.Text)
            cmd.Parameters.Add("@ppn", OleDbType.Double).Value = CDbl(Me.txtPPN.Text)
            cmd.Parameters.Add("@harga_ppn", OleDbType.Double).Value = CDbl(Me.txtHasilPPN.Text)
            cmd.Parameters.Add("@total_rp", OleDbType.Double).Value = CDbl(Me.txtTotalRp.Text)

            cmd.CommandText = simpanPenjualan
            cmd.ExecuteNonQuery()

            ' read all data from datagridview and save it into table detail penjualan
            For Each row As DataGridViewRow In dgvPenjualan.Rows
                If Not row.IsNewRow Then
                    Dim simpanDetail As String = "INSERT INTO tb_detail_penjualan ([id_penjualan], [id_barang], [qty], [bonus], [satuan], [harga_satuan], [disc], [jumlah]) " & _
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
                    cmd = New OleDbCommand(simpanDetail, conn)

                    cmd.Parameters.Add("@id_penjualan", OleDbType.VarChar).Value = Me.lblFaktur.Text
                    cmd.Parameters.Add("@id_barang", OleDbType.VarChar).Value = row.Cells(0).Value
                    cmd.Parameters.Add("@qty", OleDbType.Double).Value = CDbl(row.Cells(2).Value)
                    cmd.Parameters.Add("@bonus", OleDbType.Double).Value = CDbl(row.Cells(3).Value)
                    cmd.Parameters.Add("@satuan", OleDbType.VarChar).Value = UCase(row.Cells(4).Value)
                    cmd.Parameters.Add("@harga_satuan", OleDbType.Double).Value = CDbl(row.Cells(5).Value)
                    cmd.Parameters.Add("@disc", OleDbType.Double).Value = CDbl(row.Cells(6).Value)
                    cmd.Parameters.Add("@jumlah", OleDbType.Double).Value = CDbl(row.Cells(7).Value)

                    cmd.CommandText = simpanDetail
                    cmd.ExecuteNonQuery()
                End If

                ' menghitung stock awal dengan stock yang diretur
                Dim stock_akhir As Double = CDbl(row.Cells(8).Value) - CDbl(row.Cells(2).Value)

                ' update stock barang
                Dim updateStock As String = "UPDATE tb_barang SET " & _
                    "stock_awal = ? " & _
                    "WHERE id = ? "
                cmd = New OleDbCommand(updateStock, conn)

                cmd.Parameters.Add("@stock_awal", OleDbType.Double).Value = CDbl(stock_akhir)
                cmd.Parameters.Add("@id", OleDbType.VarChar).Value = row.Cells(0).Value

                cmd.CommandText = updateStock
                cmd.ExecuteNonQuery()
            Next

            MsgBox("Data berhasil disimpan!", MsgBoxStyle.Information, "Informasi")

            ' menampilkan form cetak resi
            frmCetakPenjualan.crvCetakPenjualan.SelectionFormula = "{tb_penjualan.id} = '" & Me.lblFaktur.Text & "'"
            frmCetakPenjualan.crvCetakPenjualan.Refresh()
            frmCetakPenjualan.ShowDialog()

            Call noFaktur()
            Call bersih()
            Call btnDefault()

            ' membersihkan DataGridView
            If Not dgvPenjualan.Rows.Count = 0 Then
                dgvPenjualan.Rows.Clear()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        ' menghapus row yang terseleksi
        For Each row As DataGridViewRow In dgvPenjualan.SelectedRows
            dgvPenjualan.Rows.Remove(row)
        Next

        Call btnDefault()
    End Sub

    Private Sub btnBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatal.Click
        Call btnDefault()
    End Sub
#End Region

#Region "Number"
    Private Sub txtQTY_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQTY.KeyPress
        ' inputan hanya bisa berupa angka dari 0 sampai 9
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        Call btnOn()
    End Sub

    Private Sub txtBonus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBonus.KeyPress
        ' inputan hanya bisa berupa angka dari 0 sampai 9
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        Call btnOn()
    End Sub

    Private Sub txtHargaSat_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHargaSat.KeyPress
        ' inputan hanya bisa berupa angka dari 0 sampai 9
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        Call btnOn()
    End Sub

    Private Sub txtDis_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDis.KeyPress
        Try
            ' inputan hanya bisa berupa angka dari 0 sampai 9
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

            ' jika masukan sama dengan 13 (Enter)
            If e.KeyChar = Convert.ToChar(13) Then
                ' menyembunyikan column Stock Akhir
                Me.dgvPenjualan.Columns("STOCK AKHIR").Visible = False

                ' jika ditekan Enter (13) TIDAK sama dengan kosong / "" 
                ' data ditambahkan ke dalam row DataGridView
                If Not Me.cmbBarang.Text = "" Or Not Me.txtQTY.Text = "" Or Not Me.txtSat.Text = "" Then
                    dgvPenjualan.Rows.Add(Me.panelIDBarang.Text, UCase(Me.cmbBarang.Text), CDbl(Me.txtQTY.Text), CDbl(Me.txtBonus.Text), UCase(Me.txtSat.Text), CDbl(Me.txtHargaSat.Text), CDbl(Me.txtDis.Text), CDbl(Me.txtJumlah.Text), CDbl(Me.panelStockAwal.Text))
                End If

                ' memanggil fungsi hitungRp()
                Me.txtRp.Text = FormatCurrency(hitungRp)

                Call btnOn()
                Call listDefault()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub txtJumlah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtJumlah.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        Call btnOn()
    End Sub

    Private Sub txtPPN_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPPN.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        ' cek if string "" or not numeric exit sub
        If Me.txtRp.Text = "" Or Not IsNumeric(Me.txtRp.Text) Then
            Exit Sub
        End If

        If Me.txtPPN.Text = "" Or Not IsNumeric(Me.txtPPN.Text) Then
            Exit Sub
        End If

        If e.KeyChar = Convert.ToChar(13) Then
            ' rumus Hasil PPN dan Total RP
            Me.txtHasilPPN.Text = FormatCurrency((CDbl(Me.txtPPN.Text) / 100) * CDbl(Me.txtRp.Text))
            Me.txtTotalRp.Text = FormatCurrency(CDbl(Me.txtRp.Text) + CDbl(Me.txtHasilPPN.Text))
        End If

        Call btnOn()
    End Sub
#End Region

#Region "Format Currency"
    Private Sub txtHargaSat_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaSat.Leave
        If Me.txtHargaSat.Text = "" Or Not IsNumeric(Me.txtHargaSat.Text) Then
            Exit Sub
        End If

        Me.txtHargaSat.Text = FormatCurrency(CDbl(Me.txtHargaSat.Text))
    End Sub

    Private Sub txtRp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRp.Leave
        Me.txtRp.Text = FormatCurrency(CDbl(Me.txtRp.Text))
    End Sub

    Private Sub txtHasilPPN_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHasilPPN.Leave
        Me.txtHasilPPN.Text = FormatCurrency(CDbl(Me.txtHasilPPN.Text))
    End Sub

    Private Sub txtTotalRp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalRp.Leave
        Me.txtTotalRp.Text = FormatCurrency(CDbl(Me.txtTotalRp.Text))
    End Sub
#End Region

    Private Sub dgvPenjualan_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPenjualan.CellContentDoubleClick
        Try
            If IsDBNull(dgvPenjualan.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                MsgBox("Tidak ada data yang dipilih!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Me.panelIDBarang.Text = dgvPenjualan.Rows(e.RowIndex).Cells(0).Value
                Me.cmbBarang.Text = dgvPenjualan.Rows(e.RowIndex).Cells(1).Value
                Me.txtQTY.Text = CDbl(dgvPenjualan.Rows(e.RowIndex).Cells(2).Value)
                Me.txtBonus.Text = CDbl(dgvPenjualan.Rows(e.RowIndex).Cells(3).Value)
                Me.txtSat.Text = dgvPenjualan.Rows(e.RowIndex).Cells(4).Value
                Me.txtHargaSat.Text = FormatCurrency(CDbl(dgvPenjualan.Rows(e.RowIndex).Cells(5).Value))
                Me.txtDis.Text = CDbl(dgvPenjualan.Rows(e.RowIndex).Cells(6).Value)
                Me.txtJumlah.Text = FormatCurrency(CDbl(dgvPenjualan.Rows(e.RowIndex).Cells(7).Value))
                Me.panelStockAwal.Text = dgvPenjualan.Rows(e.RowIndex).Cells(8).Value
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class