﻿Imports System.Data.OleDb

Public Class frmLogin

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try
            cmd = New OleDbCommand("SELECT * FROM tb_users WHERE name = '" & Me.txtUsername.Text & "' AND " & _
                "key = '" & Me.txtPassword.Text & "'", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.Visible = False
                Me.txtUsername.Clear()
                Me.txtPassword.Clear()

                frmMain.panelID.Text = UCase(dr.GetInt32(0))
                frmMain.lblNama.Text = UCase(dr.GetString(1)) ' get field name
                frmMain.lblLevel.Text = UCase(dr.GetString(3)) ' get field level

                If dr.GetString(3) <> "admin" Then
                    frmMain.DaftarBarangToolStripMenuItem.Enabled = False
                    frmMain.DaftarLanggananToolStripMenuItem.Enabled = False
                    frmMain.DaftarSalesmanToolStripMenuItem.Enabled = False
                    frmMain.DaftarSupplierToolStripMenuItem.Enabled = False
                Else
                    frmMain.DaftarBarangToolStripMenuItem.Enabled = True
                    frmMain.DaftarLanggananToolStripMenuItem.Enabled = True
                    frmMain.DaftarSalesmanToolStripMenuItem.Enabled = True
                    frmMain.DaftarSupplierToolStripMenuItem.Enabled = True
                End If

                frmMain.Show()
            Else
                MsgBox("Username dan Password tidak valid!", MsgBoxStyle.Information, "Informasi")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtUsername.Clear()
        txtPassword.Clear()
        txtUsername.Focus()
    End Sub
End Class
