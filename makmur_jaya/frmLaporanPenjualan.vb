﻿Imports System.Data.OleDb

Public Class frmLaporanPenjualan

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Me.cmbNoFaktur.Text = ""
        Me.cmbLangganan.Text = ""
    End Sub

    ' Sub untuk mengambil data dari tabel penjualan
    ' dengan menambahkan data dari tabel ke dalam cmbNoFaktur
    Sub ambilDataPenjualan()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_penjualan ORDER BY id DESC", conn)
            dr = cmd.ExecuteReader

            Do While dr.Read
                cmbNoFaktur.Items.Add(dr.GetValue(0))
            Loop
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' Sub untuk mengambil data dari tabel pelanggan
    ' dengan menambahkan data dari tabel ke dalam cmbLangganan
    Sub ambilDataLangganan()
        Try
            cmd = New OleDbCommand("SELECT nama_langganan FROM tb_pelanggan ORDER BY nama_langganan ASC", conn)
            dr = cmd.ExecuteReader

            Do While dr.Read
                cmbLangganan.Items.Add(dr.GetValue(0))
            Loop
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub frmLaporanPenjualan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call ambilDataPenjualan()
        Call ambilDataLangganan()
    End Sub

    Private Sub cmbLangganan_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLangganan.SelectedIndexChanged
        Try
            ' mengambil data id langganan berdasarkan nama langganan yang terpilih di ComboBox Langganan
            cmd = New OleDbCommand("SELECT id FROM tb_pelanggan WHERE nama_langganan = ?", conn)
            cmd.Parameters.Add("@nama_langganan", OleDbType.VarChar).Value = Me.cmbLangganan.Text

            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDLangganan.Text = dr.GetValue(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnTampil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTampil.Click
        Try
            ' mengambil data CrystalReportViewer berdasarkan SelectionFormula dengan empat kondisi
            ' kondisi (1) jika cmbNoFaktur TIDAK kosong atau ""
            ' kondisi (2) jika cmbLangganan TIDAK kosong atau ""
            ' kondisi (3) jika cmbNoFaktur Dan cmbLangganan TIDAK kosong atau ""
            ' kondisi (4) default ketika semua ComboBox dalam keadaan kosong atau ""
            If Not Me.cmbNoFaktur.Text = "" Then
                crvPenjualan.SelectionFormula = "{tb_penjualan.id} = '" & Me.cmbNoFaktur.Text & "' AND " & _
                    "{tb_penjualan.tanggal_penjualan} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_penjualan.tanggal_penjualan} <= CDate('" & Me.dtpAkhir.Value & "')"
            ElseIf Not Me.cmbLangganan.Text = "" Then
                crvPenjualan.SelectionFormula = "{tb_pelanggan.id} = '" & Me.panelIDLangganan.Text & "' AND " & _
                    "{tb_penjualan.tanggal_penjualan} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_penjualan.tanggal_penjualan} <= CDate('" & Me.dtpAkhir.Value & "')"
            ElseIf Not Me.cmbNoFaktur.Text = "" And Not Me.cmbLangganan.Text = "" Then
                crvPenjualan.SelectionFormula = "{tb_penjualan.id} = '" & Me.cmbNoFaktur.Text & "' AND {tb_pelanggan.id} = '" & Me.panelIDLangganan.Text & "' AND " & _
                    "{tb_penjualan.tanggal_penjualan} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_penjualan.tanggal_penjualan} <= CDate('" & Me.dtpAkhir.Value & "')"
            Else
                crvPenjualan.SelectionFormula = "{tb_penjualan.tanggal_penjualan} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_penjualan.tanggal_penjualan} <= CDate('" & Me.dtpAkhir.Value & "')"
            End If

            ' load file CrystalReport
            cryRPT.Load("crPenjualan.rpt")

            Call konfig_cr()

            ' tampilkan ke dalam CrystalReportViewer
            crvPenjualan.ReportSource = cryRPT
            crvPenjualan.RefreshReport()

            Call bersih()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class