﻿Imports System.Data.OleDb

Public Class frmLaporanPencapaian

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Me.cmbSalesman.Text = ""
    End Sub

    ' Sub untuk mengambil data dari tabel salesman
    ' dengan menambahkan data dari tabel ke dalam cmbSalesman
    Sub ambilDataSalesman()
        Try
            cmd = New OleDbCommand("SELECT nama_karyawan FROM tb_karyawan ORDER BY nama_karyawan ASC", conn)
            dr = cmd.ExecuteReader

            Do While dr.Read
                Me.cmbSalesman.Items.Add(dr.GetValue(0))
            Loop
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' main load
    Private Sub frmLaporanPencapaian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call ambilDataSalesman()
    End Sub

    Private Sub cmbSalesman_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSalesman.SelectedIndexChanged
        Try
            ' mengambil data id salesman berdasarkan nama salesman yang terpilih di ComboBox Salesman
            cmd = New OleDbCommand("SELECT id FROM tb_karyawan WHERE nama_karyawan = ?", conn)
            cmd.Parameters.Add("@nama_karyawan", OleDbType.VarChar).Value = Me.cmbSalesman.Text

            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDSalesman.Text = dr.GetValue(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnTampil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTampil.Click
        Try
            ' mengambil data CrystalReportViewer berdasarkan SelectionFormula dengan dua kondisi
            ' kondisi (1) jika cmbSalesman TIDAK kosong atau ""
            ' kondisi (2) default ketika semua ComboBox dalam keadaan kosong atau ""
            If Not Me.cmbSalesman.Text = "" Then
                crvSalesman.SelectionFormula = "{tb_karyawan.id} = '" & Me.panelIDSalesman.Text & "' AND " & _
                    "{tb_penjualan.tanggal_penjualan} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_penjualan.tanggal_penjualan} <= CDate('" & Me.dtpAkhir.Value & "')"
            Else
                crvSalesman.SelectionFormula = "{tb_penjualan.tanggal_penjualan} >= CDate('" & Me.dtpAwal.Value & "') AND {tb_penjualan.tanggal_penjualan} <= CDate('" & Me.dtpAkhir.Value & "')"
            End If

            ' load file CrystalReport
            cryRPT.Load("crPencapaian.rpt")

            Call konfig_cr()

            ' tampilkan ke dalam CrystalReportViewer
            crvSalesman.ReportSource = cryRPT
            crvSalesman.RefreshReport()

            Call bersih()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class