﻿Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Module modKoneksi
    Public conn As OleDbConnection
    Public cmd As OleDbCommand
    Public da As OleDbDataAdapter
    Public dr As OleDbDataReader
    Public dt As New DataTable
    Public ds As New DataSet

    Public cryRPT As New ReportDocument
    Public crLogonInfos As New TableLogOnInfos
    Public crLogonInfo As New TableLogOnInfo
    Public crConnectionInfo As New ConnectionInfo
    Public crTables As Tables

    Public Sub konfig_cr()
        With crConnectionInfo
            .ServerName = (Application.StartupPath.ToString & "\makmur_jaya.mdb")
            .DatabaseName = ""
            .UserID = ""
            .Password = ""
        End With

        crTables = cryRPT.Database.Tables
        For Each crTable As Table In crTables
            crLogonInfo = crTable.LogOnInfo
            crLogonInfo.ConnectionInfo = crConnectionInfo
            crTable.ApplyLogOnInfo(crLogonInfo)
        Next
    End Sub

    Public Sub koneksi()
        Try
            Dim strKon As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=makmur_jaya.mdb"
            conn = New OleDbConnection(strKon)

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

End Module
