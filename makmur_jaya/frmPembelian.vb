﻿Imports System.Data.OleDb

Public Class frmPembelian

#Region "Sub"
    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub bersih()
        Me.cmbBarang.Text = ""
        Me.cmbSupplier.Text = ""
        Me.txtAlamatSupplier.Clear()
        Me.txtQTY.Text = 0
        Me.txtBonus.Text = 0
        Me.txtSat.Clear()
        Me.txtHargaSat.Text = 0
        Me.txtDis.Text = 0
        Me.txtJumlah.Text = 0
        Me.txtRp.Text = 0
        Me.txtPPN.Text = 0
        Me.txtHasilPPN.Text = 0
        Me.txtTotalRp.Text = 0
        Me.cmbBarang.Focus()
    End Sub

    ' bersihkan semua TextBox atau ComboBox yang terisi
    Sub listDefault()
        Me.cmbBarang.Text = ""
        Me.txtQTY.Text = 0
        Me.txtBonus.Text = 0
        Me.txtSat.Clear()
        Me.txtHargaSat.Text = 0
        Me.txtDis.Text = 0
        Me.txtJumlah.Text = 0
        Me.txtHasilPPN.Text = 0
        Me.txtTotalRp.Text = 0
        Me.cmbBarang.Focus()
    End Sub

    ' nilai boolean untuk button saat input
    Sub btnDefault()
        Me.btnTambah.Enabled = True
        Me.btnSimpan.Enabled = False
        Me.btnHapus.Enabled = False
        Me.btnBatal.Enabled = False
    End Sub

    ' nilai boolean untuk button saat input
    Sub btnOn()
        Me.btnTambah.Enabled = False
        Me.btnSimpan.Enabled = True
        Me.btnHapus.Enabled = True
        Me.btnBatal.Enabled = True
    End Sub

    ' membuat kolom untuk DataGridView
    Sub dgvColumns()
        With dgvPembelian
            .Columns.Add("ID", "ID")
            .Columns.Add("NAMA BARANG", "NAMA BARANG")
            .Columns.Add("QTY", "QTY")
            .Columns.Add("BONUS", "BONUS")
            .Columns.Add("SATUAN", "SATUAN")
            .Columns.Add("HARGA SATUAN", "HARGA SATUAN")
            .Columns.Add("DISKON", "DISKON")
            .Columns.Add("JUMLAH", "JUMLAH")
        End With
    End Sub

    ' menghitung jumlah qty dengan harga satuan
    Function hitungJumlah(ByVal qty As Double, ByVal hargaSatuan As Double, ByVal diskon As Double) As Double
        Dim harga_awal, harga_akhir, jumlah As Double

        harga_awal = qty * hargaSatuan
        harga_akhir = (diskon / 100) * harga_awal
        jumlah = harga_awal - harga_akhir

        Return jumlah
    End Function

    ' menghitung total jumlah
    Function hitungRp() As Double
        Dim rp As Double

        For Each row As DataGridViewRow In dgvPembelian.Rows
            rp += row.Cells("JUMLAH").Value
        Next

        Return rp
    End Function

    ' generate otomatis untuk noFaktur / noRetur
    Sub noFaktur()
        Try
            cmd = New OleDbCommand("SELECT id FROM tb_pembelian WHERE id IN (SELECT MAX(id) FROM tb_pembelian) ORDER BY id ASC", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            Dim no_urut As String
            Dim hitungan As Long

            If Not dr.HasRows Then
                no_urut = Format(Me.dtpFaktur.Value, "yyyyMMdd") + "001"
            Else
                If Microsoft.VisualBasic.Left(dr.GetString(0), 8) <> Format(Me.dtpFaktur.Value, "yyyyMMdd") Then
                    no_urut = Format(Me.dtpFaktur.Value, "yyyyMMdd") + "001"
                Else
                    hitungan = dr.GetString(0) + 1
                    no_urut = Format(Me.dtpFaktur.Value, "yyyyMMdd") + Microsoft.VisualBasic.Right("000" & hitungan, 3)
                End If
            End If

            ' DataReader Close
            dr.Close()

            Me.lblFaktur.Text = no_urut
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' mengambil data id dari tabel supplier
    Sub ambilSupplier()
        Try
            cmd = New OleDbCommand("SELECT nama_supplier FROM tb_supplier ORDER BY nama_supplier ASC", conn)
            dr = cmd.ExecuteReader

            While dr.Read
                Me.cmbSupplier.Items.Add(dr.GetValue(0))
            End While

            ' DataReader Close
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    ' mengambil data id dari tabel barang
    Sub ambilBarang()
        Try
            cmd = New OleDbCommand("SELECT nama_barang FROM tb_barang ORDER BY nama_barang ASC", conn)
            dr = cmd.ExecuteReader

            While dr.Read
                Me.cmbBarang.Items.Add(dr.GetValue(0))
            End While

            ' DataReader Close
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

    ' main load
    Private Sub frmPembelian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' panggil semua Sub yang dibutuhkan ketika form tampil
        Call koneksi()
        Call bersih()
        Call btnDefault()
        Call noFaktur()
        Call ambilSupplier()
        Call ambilBarang()
        Call dgvColumns()
    End Sub

#Region "TextBox"
    Private Sub dtpFaktur_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFaktur.ValueChanged
        Call noFaktur()
    End Sub

    Private Sub txtQTY_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQTY.TextChanged
        Try
            ' cek if string "" or not numeric exit sub
            If Me.txtQTY.Text = "" Or Not IsNumeric(Me.txtQTY.Text) Then
                Exit Sub
            End If

            If Me.txtHargaSat.Text = "" Or Not IsNumeric(Me.txtHargaSat.Text) Then
                Exit Sub
            End If

            If Me.txtDis.Text = "" Or Not IsNumeric(Me.txtDis.Text) Then
                Exit Sub
            End If

            ' function of hitungJumlah(qty, harga_satuan, diskon)
            Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub txtSat_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSat.TextChanged
        Try
            ' cek jika txtSat sama dengan
            If Me.txtSat.Text = UCase("KRT") Then ' satuan_besar
                cmd = New OleDbCommand("SELECT harga_jual_besar FROM tb_barang WHERE nama_barang = ? AND satuan_besar = ?", conn)
                cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.cmbBarang.Text)
                cmd.Parameters.Add("@satuan_besar", OleDbType.VarChar).Value = UCase(Me.txtSat.Text)

                dr = cmd.ExecuteReader
                dr.Read()

                Me.txtHargaSat.Text = FormatCurrency(dr.GetValue(0))

                ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
                Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
            ElseIf Me.txtSat.Text = UCase("LSN") Or Me.txtSat.Text = UCase("BAL") Then ' satuan_tanggung
                cmd = New OleDbCommand("SELECT harga_jual_tanggung FROM tb_barang WHERE nama_barang = ? AND satuan_tanggung = ?", conn)
                cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.cmbBarang.Text)
                cmd.Parameters.Add("@satuan_tanggung", OleDbType.VarChar).Value = UCase(Me.txtSat.Text)

                dr = cmd.ExecuteReader
                dr.Read()

                Me.txtHargaSat.Text = FormatCurrency(dr.GetValue(0))

                ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
                Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
            ElseIf Me.txtSat.Text = UCase("FLS") Or Me.txtSat.Text = UCase("BOX") Or Me.txtSat.Text = UCase("PCS") Then  ' satuan_kecil
                cmd = New OleDbCommand("SELECT harga_jual_kecil FROM tb_barang WHERE nama_barang = ? AND satuan_kecil = ?", conn)
                cmd.Parameters.Add("@nama_barang", OleDbType.VarChar).Value = UCase(Me.cmbBarang.Text)
                cmd.Parameters.Add("@satuan_kecil", OleDbType.VarChar).Value = UCase(Me.txtSat.Text)

                dr = cmd.ExecuteReader
                dr.Read()

                Me.txtHargaSat.Text = FormatCurrency(dr.GetValue(0))

                ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
                Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub txtDis_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDis.TextChanged
        Try
            ' cek jika nilai sama dengan "" atau bukan angka maka abaikan
            If Me.txtQTY.Text = "" Or Not IsNumeric(Me.txtQTY.Text) Then
                Exit Sub
            End If

            If Me.txtHargaSat.Text = "" Or Not IsNumeric(Me.txtHargaSat.Text) Then
                Exit Sub
            End If

            If Me.txtDis.Text = "" Or Not IsNumeric(Me.txtDis.Text) Then
                Exit Sub
            End If

            ' memanggil fungsi hitungJumlah(qty, harga_satuan, diskon)
            Me.txtJumlah.Text = FormatCurrency(hitungJumlah(Me.txtQTY.Text, Me.txtHargaSat.Text, Me.txtDis.Text))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub txtPPN_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPPN.TextChanged
        Try
            ' cek jika nilai sama dengan "" atau bukan angka maka abaikan
            If Me.txtRp.Text = "" Or Not IsNumeric(Me.txtRp.Text) Then
                Exit Sub
            End If

            If Me.txtPPN.Text = "" Or Not IsNumeric(Me.txtPPN.Text) Then
                Exit Sub
            End If

            ' rumus Hasil PPN dan Total RP
            Me.txtHasilPPN.Text = FormatCurrency((CDbl(Me.txtPPN.Text) / 100) * CDbl(Me.txtRp.Text))
            Me.txtTotalRp.Text = FormatCurrency(CDbl(Me.txtRp.Text) + CDbl(Me.txtHasilPPN.Text))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

#Region "Combobox"
    Private Sub cmbBarang_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBarang.SelectedIndexChanged
        Try
            ' ambil ID barang
            cmd = New OleDbCommand("SELECT id FROM tb_barang WHERE nama_barang = '" & UCase(Me.cmbBarang.Text) & "'", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDBarang.Text = dr.GetValue(0)
            End If

            ' autocomplete satuan
            da = New OleDbDataAdapter("SELECT satuan_besar, satuan_tanggung, satuan_kecil FROM tb_barang WHERE nama_barang = '" & UCase(Me.cmbBarang.Text) & "' ", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "tb_barang")

            Dim col As New AutoCompleteStringCollection

            For Each row In ds.Tables(0).Rows
                col.Add(row(0).ToString)
                col.Add(row(1).ToString)
                col.Add(row(2).ToString)
            Next

            Me.txtSat.AutoCompleteSource = AutoCompleteSource.CustomSource
            Me.txtSat.AutoCompleteCustomSource = col
            Me.txtSat.AutoCompleteMode = AutoCompleteMode.Suggest

            Call btnOn()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub cmbSupplier_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSupplier.SelectedIndexChanged
        Try
            cmd = New OleDbCommand("SELECT id, alamat FROM tb_supplier WHERE nama_supplier = '" & UCase(Me.cmbSupplier.Text) & "'", conn)
            dr = cmd.ExecuteReader
            dr.Read()

            If dr.HasRows Then
                Me.panelIDSupplier.Text = dr.GetValue(0)
                Me.txtAlamatSupplier.Text = dr.GetValue(1)
            End If

            Call btnOn()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

#Region "Button"
    Private Sub btnTambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTambah.Click
        Call bersih()
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Try
            ' save it into table penjualan
            Dim simpanPenjualan As String = "INSERT INTO tb_pembelian (id, tanggal_pembelian, id_supplier, rp, ppn, harga_ppn, total_rp) " & _
                "VALUES ('" & Me.lblFaktur.Text & "', #" & Me.dtpFaktur.Value & "#, '" & Me.panelIDSupplier.Text & "', " & _
                "'" & CDbl(Me.txtRp.Text) & "', '" & CDbl(Me.txtPPN.Text) & "', '" & CDbl(Me.txtHasilPPN.Text) & "', '" & CDbl(Me.txtTotalRp.Text) & "')"
            cmd = New OleDbCommand(simpanPenjualan, conn)
            cmd.ExecuteNonQuery()

            ' read all data from datagridview and save it into table detail penjualan
            For Each row As DataGridViewRow In dgvPembelian.Rows
                If Not row.IsNewRow Then
                    Dim simpanDetailPenjualan As String = "INSERT INTO tb_detail_pembelian (id_pembelian, id_barang, qty, bonus, satuan, harga_satuan, disc, jumlah) " & _
                        "VALUES ('" & Me.lblFaktur.Text & "', '" & UCase(row.Cells(0).Value) & "', '" & CDbl(row.Cells(2).Value) & "', '" & CDbl(row.Cells(3).Value) & "', " & _
                        "'" & UCase(row.Cells(4).Value) & "', " & CDbl(row.Cells(5).Value) & ", '" & CDbl(row.Cells(6).Value) & "', '" & CDbl(row.Cells(7).Value) & "')"
                    cmd = New OleDbCommand(simpanDetailPenjualan, conn)
                    cmd.ExecuteNonQuery()
                End If
            Next

            MsgBox("Data berhasil disimpan!", MsgBoxStyle.Information, "Informasi")

            ' menampilkan form cetak resi
            frmCetakPembelian.crvCetakPembelian.SelectionFormula = "{tb_pembelian.id} = '" & Me.lblFaktur.Text & "'"
            frmCetakPembelian.crvCetakPembelian.Refresh()
            frmCetakPembelian.ShowDialog()

            Call noFaktur()
            Call bersih()
            Call btnDefault()

            ' membersihkan DataGridView
            If Not dgvPembelian.Rows.Count = 0 Then
                dgvPembelian.Rows.Clear()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        For Each row As DataGridViewRow In dgvPembelian.SelectedRows
            dgvPembelian.Rows.Remove(row)
        Next

        Call btnDefault()
    End Sub

    Private Sub btnBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatal.Click
        Call btnDefault()
    End Sub
#End Region

#Region "Number"
    Private Sub txtQTY_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQTY.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        Call btnOn()
    End Sub

    Private Sub txtBonus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBonus.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        Call btnOn()
    End Sub

    Private Sub txtHargaSat_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHargaSat.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        Call btnOn()
    End Sub

    Private Sub txtDis_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDis.KeyPress
        Try
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

            If e.KeyChar = Convert.ToChar(13) Then
                ' jika ditekan Enter (13) TIDAK sama dengan kosong / "" 
                ' data ditambahkan ke dalam row DataGridView
                If Not Me.cmbBarang.Text = "" Or Not Me.txtQTY.Text = "" Or Not Me.txtSat.Text = "" Then
                    dgvPembelian.Rows.Add(Me.panelIDBarang.Text, UCase(Me.cmbBarang.Text), CDbl(Me.txtQTY.Text), CDbl(Me.txtBonus.Text), UCase(Me.txtSat.Text), CDbl(Me.txtHargaSat.Text), CDbl(Me.txtDis.Text), CDbl(Me.txtJumlah.Text))
                End If

                ' memanggil fungsi hitungRp()
                Me.txtRp.Text = FormatCurrency(hitungRp)

                Call listDefault()
                Call btnOn()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub

    Private Sub txtJumlah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtJumlah.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

        Call btnOn()
    End Sub

    Private Sub txtPPN_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPPN.KeyPress
        Try
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True

            ' cek if string "" or not numeric exit sub
            If Me.txtRp.Text = "" Or Not IsNumeric(Me.txtRp.Text) Then
                Exit Sub
            End If

            If Me.txtPPN.Text = "" Or Not IsNumeric(Me.txtPPN.Text) Then
                Exit Sub
            End If

            If e.KeyChar = Convert.ToChar(13) Then
                ' rumus Hasil PPN dan Total RP
                Me.txtHasilPPN.Text = FormatCurrency((CDbl(Me.txtPPN.Text) / 100) * CDbl(Me.txtRp.Text))
                Me.txtTotalRp.Text = FormatCurrency(CDbl(Me.txtRp.Text) + CDbl(Me.txtHasilPPN.Text))
            End If

            Call btnOn()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
#End Region

#Region "Format Currency"
    Private Sub txtHargaSat_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaSat.Leave
        If Me.txtHargaSat.Text = "" Or Not IsNumeric(Me.txtHargaSat.Text) Then
            Exit Sub
        End If

        Me.txtHargaSat.Text = FormatCurrency(CDbl(Me.txtHargaSat.Text))
    End Sub

    Private Sub txtRp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRp.Leave
        Me.txtRp.Text = FormatCurrency(CDbl(Me.txtRp.Text))
    End Sub

    Private Sub txtHasilPPN_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHasilPPN.Leave
        Me.txtHasilPPN.Text = FormatCurrency(CDbl(Me.txtHasilPPN.Text))
    End Sub

    Private Sub txtTotalRp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalRp.Leave
        Me.txtTotalRp.Text = FormatCurrency(CDbl(Me.txtTotalRp.Text))
    End Sub
#End Region

    Private Sub dgvPembelian_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPembelian.CellContentDoubleClick
        Try
            If IsDBNull(dgvPembelian.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                MsgBox("Tidak ada data yang dipilih!", MsgBoxStyle.Information, "Informasi")
                Exit Sub
            Else
                Me.panelIDBarang.Text = dgvPembelian.Rows(e.RowIndex).Cells(0).Value
                Me.cmbBarang.Text = dgvPembelian.Rows(e.RowIndex).Cells(1).Value
                Me.txtQTY.Text = CDbl(dgvPembelian.Rows(e.RowIndex).Cells(2).Value)
                Me.txtBonus.Text = CDbl(dgvPembelian.Rows(e.RowIndex).Cells(3).Value)
                Me.txtSat.Text = dgvPembelian.Rows(e.RowIndex).Cells(4).Value
                Me.txtHargaSat.Text = FormatCurrency(CDbl(dgvPembelian.Rows(e.RowIndex).Cells(5).Value))
                Me.txtDis.Text = CDbl(dgvPembelian.Rows(e.RowIndex).Cells(6).Value)
                Me.txtJumlah.Text = FormatCurrency(CDbl(dgvPembelian.Rows(e.RowIndex).Cells(7).Value))
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Informasi")
        End Try
    End Sub
End Class