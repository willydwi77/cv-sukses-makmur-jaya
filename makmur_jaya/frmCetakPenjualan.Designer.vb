﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCetakPenjualan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvCetakPenjualan = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.cetakPenjualan1 = New makmur_jaya.cetakPenjualan()
        Me.SuspendLayout()
        '
        'crvCetakPenjualan
        '
        Me.crvCetakPenjualan.ActiveViewIndex = 0
        Me.crvCetakPenjualan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvCetakPenjualan.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvCetakPenjualan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvCetakPenjualan.Location = New System.Drawing.Point(0, 0)
        Me.crvCetakPenjualan.Name = "crvCetakPenjualan"
        Me.crvCetakPenjualan.ReportSource = Me.cetakPenjualan1
        Me.crvCetakPenjualan.ShowGotoPageButton = False
        Me.crvCetakPenjualan.ShowGroupTreeButton = False
        Me.crvCetakPenjualan.ShowPageNavigateButtons = False
        Me.crvCetakPenjualan.ShowParameterPanelButton = False
        Me.crvCetakPenjualan.ShowTextSearchButton = False
        Me.crvCetakPenjualan.ShowZoomButton = False
        Me.crvCetakPenjualan.Size = New System.Drawing.Size(908, 389)
        Me.crvCetakPenjualan.TabIndex = 1
        Me.crvCetakPenjualan.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'frmCetakPenjualan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(908, 389)
        Me.Controls.Add(Me.crvCetakPenjualan)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCetakPenjualan"
        Me.Text = "Cetak Penjualan"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvCetakPenjualan As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents cetakPenjualan1 As makmur_jaya.cetakPenjualan
End Class
